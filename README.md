# ISOMETRIC_ENGINE - THIS README IS WIP


## Description
This is a Isometric engine (and eventually a game) built with C++17 and SFML
Library. 

Art by: <b>© Dorota Zimová</b> (Accepts commisions at <a 
                     href="https://ko-fi.com/panizima"> ko-fi.com</a>)

Code by: <b>Martin Pavelka</b> contact@mpavelka.eu

## Licensing
The source code is licensed as MIT. However <b>the art is not.</b>

The art is licensed under 
<a href="https://creativecommons.org/licenses/by-nc-nd/4.0/">
<b>Attribution-NonCommercial-NoDerivatives 4.0 International 
(CC BY-NC-ND 4.0)</b></a>. Which permits personal use without modifying. 


## Requirements
* cmake
* SFML 2.6.1 (Included in source files) 
* gcc for linux
* visual studio for windows

## Building
LINUX:<br>
```
cd isometric_engine && mkdir build && cd build
cmake ../src -DBUILD_TARGET=LIN 
cd <executable> && make
```
<br>
Windows:<br>
Open Visual studio x64 command line and run: (for now you also need to move the
executable from Debug one directory higher, will be fixed in the future<br>

```
cd isometric_engine && mkdir build && cd build
cmake ../src -DBUILD_TARGET=WIN
cd <executable> && make
```

For example:
```
cmake ../src && cd tilemap_builder && make
```

This will create the executable.
To run the executable do:
```
./tilemap_builder 
```

## Contributing
For now no contributions are accepted

## Tilemap Builder Controlls
* WASD - movement
* Scroll Wheel - Zoom
* Left Click - Place tile
* open tile create, and select tiles to place from Existing Tiles


<img alt="Demo Screenshot 1" src="https://mpavelka.eu/gitlab_images/iso_readme_screen1.png">
<img alt="Demo Screenshot 2" src="https://mpavelka.eu/gitlab_images/iso_readme_screen2.png">
