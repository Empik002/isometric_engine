#pragma once

#include "sf.hpp"
#include <SFML/System/Vector2.hpp>

// TODO: this file should not exist move this to proper files

namespace ise{

sf::Vector2<unsigned int> get_window_center(const sf::RenderWindow& window);
void set_mouse_to_center(const sf::RenderWindow& window);
sf::Vector2f adjusted_mouse_pos(sf::RenderWindow& window);
sf::Vector2f round(sf::Vector2f vec);

} // namespace ise
