#include <drag_and_drop.hpp>


namespace ise{


#ifdef LIN


DND_state DND_init(DNDRenderWindow& window){
    return XDNDinit(window);
}

std::vector<std::string> DND_update(DND_state& state){
    return XDNDupdate(state);
}

void DND_exit(DND_state& state){
    XDNDexit(state);
}


#endif


#ifdef WIN

DND_state DND_init(DNDRenderWindow& window){
    DND_state state = WINDND_init(window);
    //std::cout << "second " <<  state.callback << std::endl;
    return state;
}

std::vector<std::string> DND_update(DND_state& state){
    return WINDND_update(state);
}

void DND_exit(DND_state& state){
    WINDND_update(state);
}

#endif

} // namespace ise
