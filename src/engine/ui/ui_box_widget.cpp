#include "ui_box_widget.hpp"

#include <SFML/Graphics/Rect.hpp>
#include <SFML/Graphics/RenderStates.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/System/Vector2.hpp>
#include <SFML/Window/Event.hpp>
#include <SFML/Window/Mouse.hpp>

#include "ui_manager.hpp"
#include "ui_window.hpp"
#include "utilities.hpp"

namespace ise {
namespace ui {

bool UIBoxWidget::process_click_event(sf::Event event) {
    // exit if not over the widget
    if (!contains(event.mouseButton.x, event.mouseButton.y)) {
        was_pressed = false;
        return false;
    }

    if (event.type == event.MouseButtonPressed &&
        event.mouseButton.button == sf::Mouse::Left) {
        was_pressed = true;
    }

    else if (event.type == event.MouseButtonReleased &&
             event.mouseButton.button == sf::Mouse::Left) {
        if (was_pressed) {
            clicked = true;
        }
    }
    return true;
}

void UIBoxWidget::set_relative_position(sf::Vector2<float> pos){
    _rect.left = pos.x;
    _rect.top = pos.y;
}

bool UIBoxWidget::process_event(const sf::Event& event) {
    if (event.type == event.MouseButtonPressed ||
        event.type == event.MouseButtonReleased) {
        return process_click_event(event);
    }
    return false;
}

bool UIBoxWidget::contains(float x, float y) { return contains({x, y}); }
bool UIBoxWidget::contains(sf::Vector2<float> pos) {
    // adjust relative position
    sf::Vector2<float> abs_pos =
        get_parent_window()->get_absolute_pos({_rect.left, _rect.top});
    return sf::Rect<float>(abs_pos.x, abs_pos.y, _rect.width, _rect.height)
        .contains(pos);
}

void UIBoxWidget::update(sf::RenderWindow&) {
    // if not hovering on the widget connot be clicked
    // or if not was pressed (if it is clicked it stays pressed)
    // then it gets unclicked, which next frame triggers this
    if (!was_pressed) {
        clicked = false;
        was_pressed = false;
    }

    if (clicked && was_pressed) {
        was_pressed = false;
    }
}

bool UIBoxWidget::is_clicked() const { return clicked; }
bool UIBoxWidget::is_pressed() const { return was_pressed && !clicked; }

}  // namespace ui
}  // namespace ise
