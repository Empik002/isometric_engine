#pragma once

#include <string>

#include "sf.hpp"
#include "ui_box_widget.hpp"
#include "ui_manager.hpp"
#include "ui_styles.hpp"
#include "ui_window.hpp"

namespace ise {
namespace ui {

class TextInput : public UIBoxWidget {
   protected:
    std::string* _out;
    // character limit for the string, zero means unlimited text
    unsigned int _char_limit = 0;


    // variable set when an event is processed and the text changed
    // here to not have to rerender text every frame, only when changed
    // set to true the first time, just to render the text that is in the 
    // _out variable when creating this widget
    bool updated = true;

    // this variable is set to true if 
    bool focused = false;

    sf::Text _text;

   private:
    TextInputStyle _style;
    sf::RectangleShape bg;

    void reset_pos();

   protected:
    virtual bool add_character(const sf::Event& event);

   public:
    TextInput(sf::Rect<float> rect, std::string& text, int char_limit,
              TextInputStyle = {});

    void set_style(TextInputStyle style);
    TextInputStyle get_style() const;

    bool is_focused() const;

    virtual bool process_event(const sf::Event& event) override;
    virtual void update(sf::RenderWindow& window) override;
    virtual void draw(sf::RenderWindow& window) override;
};

}  // namespace ui
}  // namespace ise
