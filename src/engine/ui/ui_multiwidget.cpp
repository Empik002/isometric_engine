#include "ui_multiwidget.hpp"

#include "ui_window.hpp"

namespace ise {
namespace ui {

void UIMultiWidget::set_parent_window(UIBaseWindow* parent_window) {
    // set parent window for the actuall widget
    _parent_window = parent_window;

    // set window for the grouped widgets (but set it as this widget)
    // because it is derived from UIBaseWindow, so it implements
    // the required function
    for (UIWidget* w : widgets) {
        w->set_parent_window(this);
    }
}

void UIMultiWidget::set_parent_window(UIBaseWindow& parent_window) {
    set_parent_window(&parent_window);
}

bool UIMultiWidget::add_widget(UIWidget& widget) {
    // add widget
    auto inserted = widgets.insert(&widget);
    if (!inserted.second) return false;

    // set its parent window
    widget.set_parent_window(get_parent_window());

    return true;
}

void UIMultiWidget::update(sf::RenderWindow& window) {
    for (UIWidget* w : widgets) {
        w->update(window);
    }
}

void UIMultiWidget::draw(sf::RenderWindow& window) {
    for (UIWidget* w : widgets) {
        w->draw(window);
    }
}

bool UIMultiWidget::process_event(const sf::Event& event) {
    return std::any_of(widgets.begin(), widgets.end(),
                       [event](auto w) { return w->process_event(event); });
}

sf::Vector2<float> UIMultiWidget::get_absolute_pos(sf::Vector2<float> pos) {
    return get_parent_window()->get_absolute_pos(_pos) + pos;
}

bool UIMultiWidget::is_focused() const {
    return get_parent_window()->is_focused();
}

void UIMultiWidget::set_relative_position(sf::Vector2<float> pos) {
    _pos = pos;
}

}  // namespace ui
}  // namespace ise
