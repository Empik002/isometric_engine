#pragma once

#include <SFML/Graphics/Color.hpp>
#include <SFML/Graphics/RenderWindow.hpp>

#include "sf.hpp"
#include "ui_box_widget.hpp"
#include "ui_manager.hpp"
#include "utilities.hpp"
#include "ui_styles.hpp"

namespace ise {
namespace ui {


class Button : public UIBoxWidget {
    sf::RectangleShape bg;
    sf::Text* text;
    ButtonStyle _style;

    void reset_pos();

   public:
    virtual ~Button() {}
    Button(sf::Rect<float> rect, sf::Text& button_text, ButtonStyle style = {});
    
    void set_style(ButtonStyle style);
    ButtonStyle get_style() const;

    void draw(sf::RenderWindow& window) override;
};

}  // namespace ui
}  // namespace ise
