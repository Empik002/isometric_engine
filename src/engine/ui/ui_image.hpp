#include <SFML/Graphics/Color.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/System/Vector2.hpp>

#include "animated_sprite.hpp"
#include "sf.hpp"
#include "ui_manager.hpp"
#include "ui_widget.hpp"
#include "ui_window.hpp"
#include "utilities.hpp"

namespace ise {
namespace ui {

class Image : public UIWidget {
    sf::Vector2<float> _pos;
    ise::AnimatedSprite* _sprite;

   public:
    virtual ~Image() {}
    Image(sf::Vector2<float> relative_pos, ise::AnimatedSprite& sprite) 
        : _pos(relative_pos), _sprite(&sprite) {}
    
    void set_relative_position(sf::Vector2<float> pos) override;

    void draw(sf::RenderWindow& window) override;
};

}  // namespace ui
}  // namespace ise
