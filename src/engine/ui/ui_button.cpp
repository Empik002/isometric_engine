#include "ui_button.hpp"

#include <SFML/Graphics/Color.hpp>
#include <SFML/Graphics/Rect.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/System/Vector2.hpp>
#include <SFML/Window/Event.hpp>
#include <SFML/Window/Mouse.hpp>

#include "log.hpp"
#include "ui_box_widget.hpp"
#include "ui_manager.hpp"
#include "ui_widget.hpp"
#include "ui_window.hpp"
#include "utilities.hpp"

namespace ise {
namespace ui {

Button::Button(sf::Rect<float> rect, sf::Text &button_text,
               ButtonStyle style /* = default style*/)
    : UIBoxWidget(rect),
      bg(sf::Vector2f(rect.width, rect.height)),
      text(&button_text),
      _style(style) {
    bg.setFillColor(_style.bg_color);
    bg.setOutlineColor(_style.outline_color);
    bg.setOutlineThickness(_style.outline_thickness);
}

void Button::reset_pos() {
    bg.setPosition(
        get_parent_window()->get_absolute_pos({_rect.left, _rect.top}));

    // get center of button
    float cx = bg.getPosition().x + _rect.width / 2;
    float cy = bg.getPosition().y + _rect.height / 2;

    // get center of text
    sf::FloatRect text_rect = text->getGlobalBounds();
    float tcx = text_rect.width / 2;
    float tcy = text_rect.height / 2;

    float lbx = tcx + text->getLocalBounds().left;
    float lby = tcy + text->getLocalBounds().top;
    text->setOrigin(round({lbx, lby}));

    // add the relative offsets to position
    text->setPosition({cx, cy});
}

void Button::draw(sf::RenderWindow &window) {
    reset_pos();
    if (is_pressed()) {
        bg.setFillColor(_style.bg_pressed_color);
    } else if (get_parent_window()->is_focused() &&
               contains(ise::adjusted_mouse_pos(window))) {
        bg.setFillColor(_style.bg_hover_color);
    } else {
        bg.setFillColor(_style.bg_color);
    }

    window.draw(bg);
    window.draw(*text);
}

void Button::set_style(ButtonStyle style) { _style = style; }
ButtonStyle Button::get_style() const { return _style; }

}  // namespace ui
}  // namespace ise
