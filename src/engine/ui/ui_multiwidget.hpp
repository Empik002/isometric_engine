#pragma once

#include <SFML/System/Vector2.hpp>
#include <vector>

#include "ui_manager.hpp"
#include "ui_widget.hpp"
#include "ui_base_window.hpp"
#include "ui_window.hpp"

namespace ise {
namespace ui {

/**
 * This is a base class for groups of widgets, it functions very
 * simillarly to a window, it has a list of pointers to widgets,
 * it executes their update, and draw functions etc, the idea is
 * that when you want to create a template for a group of widgets,
 * for example for integer input, you can have a text input and two
 * buttons with + and - to enable mouse input of numbers.
 * You would create a class NumberInput that inherits from this class
 * then in the constructor you would use the add_widget function of the
 * base class to add the desired widgets (their positions are relative to their
 * UIMultiWidget base position (they will get translated to relative to window
 * in the add_widget postion) then in the update function you would first,
 * call the base classes update (so that the events are processed properly),
 * and then you would handle the inputs of the widgets, e.g. if the +
 * button was clicked you add 1 to the input and so on.
 *
 */
class UIMultiWidget : public UIWidget, public UIBaseWindow {
    sf::Vector2<float> _pos;
    std::set<UIWidget*> widgets;

   public:
    UIMultiWidget(sf::Vector2<float> pos) : _pos(pos){}
    ~UIMultiWidget(){};

    /**
     * @brief Overrides basic set_parent_window, sets the parent window
     * to this widget,and sets this widget as parent to all of the widgets, 
     * included in widgets vector, the reference function is not overriden as 
     * it just calls this function
     *
     * @param parent_window
     */
    virtual void set_parent_window(UIBaseWindow* parent_window) override;
    virtual void set_parent_window(UIBaseWindow& parent_window) override;

    virtual void set_relative_position(sf::Vector2<float> pos) override;

    /**
     * @brief Add a widget to the vector, also set the widgets parent to
     * this widget
     *
     * @param widget
     */
    virtual bool add_widget(UIWidget& widget) override;


    virtual sf::Vector2<float> get_absolute_pos(
        sf::Vector2<float> pos) override;
    
    virtual bool is_focused() const override;

    virtual bool process_event(const sf::Event& event) override;
    virtual void update(sf::RenderWindow& window) override;
    virtual void draw(sf::RenderWindow& window) override;
};

// TODO: remove this, this is only for testing
class TestMultiWidget : public UIMultiWidget{
    sf::Text test_button_text;
    ise::ui::Button test_button{{0, 0, 75, 45}, test_button_text};
    ise::ui::Button test_button2{{76, 0, 75, 15}, test_button_text};

    public:
    TestMultiWidget(sf::Vector2<float> pos) : UIMultiWidget(pos){
        add_widget(test_button);
        add_widget(test_button2);
    }
};

}  // namespace ui
}  // namespace ise
