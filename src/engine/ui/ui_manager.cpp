#include "ui_manager.hpp"

#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Window/Event.hpp>

#include "ui_button.hpp"
#include "ui_base_window.hpp"
#include "ui_window.hpp"

namespace ise {
namespace ui {

// =========== UI ============

// set default font to default constructor
sf::Font UI::default_font = {};

void UI::set_default_font(const sf::Font& font){
    default_font = font;
}
    
sf::Font* UI::get_default_font(){
    return &default_font;
}

void UI::add_window(UIWindow& window) {
    windows.push_back(&window);
    window.parent = this;
}

void UI::remove_window(UIWindow& window) {
    windows.erase(std::find(windows.begin(), windows.end(), &window));
    window.parent = nullptr;
}

void UI::focus_window(UIWindow& window) {
    // if the window is unmovable do not reorder it, we do not want 
    // unmovable windows to end up covering movable windows
    if (window.is_movable()){
        remove_window(window);
        add_window(window);
    }
    focused_window = &window;
}

void UI::unfocus_window() { focused_window = nullptr; }

bool UI::has_focus() const{
    return focused_window != nullptr;
}

bool UI::handle_mouse_event(const sf::Event& event) {
    if (event.type == sf::Event::MouseButtonPressed &&
        event.mouseButton.button == sf::Mouse::Left) {
        handle_mouse_left_pressed(event);
    }else if (event.type == sf::Event::MouseButtonReleased &&
        event.mouseButton.button == sf::Mouse::Left){
        handle_mouse_left_released(event);
    } 
    return false;
}

bool UI::handle_mouse_left_released(const sf::Event& event) {
    if (has_focus()){
        focused_window->process_event(event);
        //always retrun true on left click / release events
        return true;
    }
    return false;
}

bool UI::handle_mouse_left_pressed(const sf::Event& event) {
    // NOTE: think about this a little bit, the has_focus() in here 
    // creates a little dilemma when having an unmovable window focused
    // as they do not move to the top when clicked, meaning if we move 
    // a window partially over an unmovable window and focus the unmovable
    // then try to focus the movable in a place that intersects the unmovable
    // window it wont get focused, instead it will be clikced through
    // Therefore I am skipping this autohandoff if the focused window
    // is unmovable, might come back and bite me in the ass, only time will
    // see lol. (the focused_window->is_movable() acomplishes this
    //
    // First check if we are clicking in the focused window;
    if (has_focus() && focused_window->is_movable() &&
        focused_window->contains(event.mouseButton.x,
                                 event.mouseButton.y)) {
        // hand off button handling to focused window
        focused_window->process_event(event);
        //always retrun true on left click / release events
        return true;
    }

    // Otherwise check if a windows was clicked then focus it and handoff
    // Loop in reverse order so that we ensure that the top most window
    // recieves the event
    for (auto window_it = windows.rbegin(); 
         window_it != windows.rend();
         ++window_it) {

        // if a window was clicked
        if ((*window_it)->contains(event.mouseButton.x, 
                    event.mouseButton.y)) {
            // save the pointer, because the iterator will get invalidated by 
            // the focusing;
            UIWindow* window_p = *window_it;
            // focus the window 
            focus_window(*window_p);
            // handoff the event
            window_p->process_event(event);

            //always retrun true on left click / release events
            return true;
        }
    }

    // if a window wasnt clicked, unfocus window but do not "consume" the
    // event, BUT first give the event to the focused window!
    if (has_focus()){
        focused_window->process_event(event);
    }
    unfocus_window();
    return false;
}

bool UI::handle_mouse_move(const sf::Event&) { return false; }

bool UI::process_event(const sf::Event& event, sf::RenderWindow& window) {
    switch (event.type) {
        case sf::Event::MouseMoved:
            return handle_mouse_move(event);
        case sf::Event::KeyPressed:  // fall through
        case sf::Event::KeyReleased:
        case sf::Event::TextEntered:
            // TODO: this is temporary
            if (focused_window != nullptr){return focused_window->process_event(event);}
            return false;
            //return handle_key_event(event);
        case sf::Event::MouseButtonPressed:   // fall through
        case sf::Event::MouseButtonReleased:  // fall through
        case sf::Event::MouseWheelScrolled:
            return handle_mouse_event(event);
        case sf::Event::Resized:
            // TODO: process the view here, for now nothing
            /*
             *ui_view.setSize(window.getSize().x, window.getSize().y);
             *ui_view.setSize(event.size.width, event.size.height);
             */
            return false;
        default:
            return false;
    }
}

bool UI::is_focused(const UIWindow& window) const {
    return focused_window == &window;
}

void UI::begin_view(sf::RenderWindow& window){
    // store the old view
    original_view = window.getView();
    // set custom
    window.setView(ui_view);
}

void UI::set_ui_view(const sf::View& view){
    ui_view = view;
}

void UI::end_view(sf::RenderWindow& window){
    // set original
    window.setView(original_view);
}

void UI::update(sf::RenderWindow &render_window ){
    // set ui view
    begin_view(render_window);

    for (UIWindow* window : windows) {
        window->update(render_window);
    }
    
    // restore old view
    end_view(render_window);

}

void UI::draw(sf::RenderWindow& render_window) {
    // set ui view
    begin_view(render_window);

    for (UIWindow* window : windows) {
        window->draw(render_window);
    }

    // restore old view
    end_view(render_window);
}

UI::UI(const sf::RenderWindow& window){
    // set the default ui_veiw settings
    ui_view.setSize(window.getSize().x, window.getSize().y);
    ui_view.setCenter(window.getSize().x / 2.f,
                      window.getSize().y / 2.f);
}

        

}  // namespace ui
}  // namespace ise
