#pragma once
#include "sf.hpp"
#include "ui_manager.hpp"

namespace ise {
namespace ui {
// some consts - Check below for a list of some presets

// COMMON VARAIABLES
const sf::Color WIDGET_BACKGROUND_COLOR{130, 70, 85};
const sf::Color WIDGET_HOVER_BACKGROUND_COLOR{130, 90, 85};
const sf::Color WIDGET_PRESSED_BACKGROUND_COLOR{130, 120, 136};
const sf::Color WIDGET_OUTLINE_COLOR{0, 0, 85};
const sf::Color TRANSPARENT{0, 0, 0, 0};
const float WIDGET_OUTLINE_THICKNESS = 1;

const float CHECKBOX_SIZE_RATIO = 0.8;

// FORWARD DECLARATIONS
/*
 *struct UIWindowStyle;
 *struct ButtonStyle;
 *struct CheckBoxStyle;
 */

/**
 *  @brief All Ui Styles are initialized with the struct initialization
 *  eg:  UIWindowStyle example_style = {.bg_color = {25,15,255}}
 *  This creates window style with bg color {25,15,255}
 *  and default value for everyhing else
 */

struct ButtonStyle {
    sf::Color bg_color = WIDGET_BACKGROUND_COLOR;
    sf::Color bg_hover_color = WIDGET_HOVER_BACKGROUND_COLOR;
    sf::Color bg_pressed_color = WIDGET_PRESSED_BACKGROUND_COLOR;
    sf::Color outline_color = WIDGET_OUTLINE_COLOR;
    float outline_thickness = WIDGET_OUTLINE_THICKNESS;
};

struct ImageButtonStyle {
    sf::Color bg_color = WIDGET_BACKGROUND_COLOR;
    sf::Color bg_hover_color = WIDGET_HOVER_BACKGROUND_COLOR;
    sf::Color bg_pressed_color = WIDGET_PRESSED_BACKGROUND_COLOR;
    sf::Color outline_color = WIDGET_OUTLINE_COLOR;
    float outline_thickness = WIDGET_OUTLINE_THICKNESS;
};

struct CheckBoxStyle {
    sf::Color bg_color = WIDGET_BACKGROUND_COLOR;
    sf::Color checked_color{0, 0, 0};
    sf::Color outline_color = WIDGET_OUTLINE_COLOR;
    float outline_thickness = WIDGET_OUTLINE_THICKNESS;
    float size_ratio = CHECKBOX_SIZE_RATIO;
};

struct UIWindowStyle {
    sf::Color bg_color{149, 170, 202};
    sf::Color outline_color{0, 0, 0};
    float movebar_height = 35;
    float outline_thickness = 1;
    ButtonStyle movebar_style{};
};

struct TextInputStyle {
    sf::Font* font = UI::get_default_font();
    sf::Color bg_color = WIDGET_BACKGROUND_COLOR;
    sf::Color bg_hover_color = WIDGET_HOVER_BACKGROUND_COLOR;
    sf::Color bg_pressed_color = WIDGET_PRESSED_BACKGROUND_COLOR;
    sf::Color outline_color = WIDGET_OUTLINE_COLOR;
    float outline_thickness = WIDGET_OUTLINE_THICKNESS;
};

// SOME PRESETS
const UIWindowStyle DEFAULT_WINDOW{};
const UIWindowStyle TRANSPARENT_WINDOW{.bg_color = TRANSPARENT,
                                       .outline_color = TRANSPARENT};

}  // namespace ui
}  // namespace ise
