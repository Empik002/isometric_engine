#include "ui_checkbox.hpp"

#include <SFML/Graphics/RenderWindow.hpp>

#include "ui_box_widget.hpp"
#include "ui_manager.hpp"
#include "ui_window.hpp"
#include "utilities.hpp"

namespace ise {
namespace ui {

CheckBox::CheckBox(sf::Rect<float> rect, bool &value,
                   CheckBoxStyle style /* default style */)
    : UIBoxWidget(rect),
      bg(sf::Vector2f(rect.width, rect.height)),
      check(sf::Vector2f(rect.width * style.size_ratio,
                         rect.height * style.size_ratio)),
      _checked(&value),
      _style(style) {
    bg.setFillColor(_style.bg_color);
    bg.setOutlineColor(_style.outline_color);
    bg.setOutlineThickness(_style.outline_thickness);
    check.setFillColor(_style.checked_color);
}

void CheckBox::reset_pos() {
    bg.setPosition(
        get_parent_window()->get_absolute_pos({_rect.left, _rect.top}));

    // get offset for the check part
    //                                    // the padding is half of the
    //                                    remaining size
    sf::Vector2f offset = bg.getSize() * ((1 - _style.size_ratio) / 2);

    check.setPosition(bg.getPosition() + offset);
}

void CheckBox::update(sf::RenderWindow &window) {
    UIBoxWidget::update(window);
    if (is_clicked()) {
        *_checked = !(*_checked);
    }
}

void CheckBox::draw(sf::RenderWindow &window) {
    reset_pos();

    window.draw(bg);
    if (is_checked()) {
        window.draw(check);
    }
}

bool CheckBox::is_checked() const { return *_checked; }

void CheckBox::set_style(CheckBoxStyle style) { _style = style; }
CheckBoxStyle CheckBox::get_style() const { return _style; }

}  // namespace ui
}  // namespace ise
