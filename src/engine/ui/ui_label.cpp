#include "ui_label.hpp"

#include "ui_manager.hpp"
#include "ui_widget.hpp"
#include "ui_window.hpp"

namespace ise {
namespace ui {

void Label::set_relative_position(sf::Vector2<float> pos) { _pos = pos; }

void Label::draw(sf::RenderWindow& window) {
    _text->setPosition(get_parent_window()->get_absolute_pos(_pos));
    window.draw(*_text);
}
}  // namespace ui
}  // namespace ise
