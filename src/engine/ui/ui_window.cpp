#include "ui_window.hpp"

#include <SFML/Graphics/Rect.hpp>
#include <SFML/Graphics/Text.hpp>
#include <SFML/System/Vector2.hpp>
#include <algorithm>
#include <cstdio>

#include "ui_manager.hpp"
#include "utilities.hpp"

namespace ise {
namespace ui {

UIWindow::UIWindow(sf::Rect<float> rect, sf::Text *name /*= nullptr*/,
                   bool is_movable /*=true*/,
                   UIWindowStyle style /*= default style*/)
    : _rect(rect),
      bg(sf::Vector2f(_rect.width, _rect.height)),
      _is_movable(is_movable),
      _style(style) {
    bg.setFillColor(_style.bg_color);
    bg.setPosition(_rect.left, _rect.top);
    bg.setOutlineColor(_style.outline_color);
    bg.setOutlineThickness(_style.outline_thickness);

    // add a move bar if the window is movable
    if (is_movable) {
        move_bar = {
            {0, -_style.movebar_height, rect.width, _style.movebar_height},
            name == nullptr ? default_button_text : *name};
    } else {
        // set movebar height to 0, if we cannot move the window
        _style.movebar_height = 0;
    }

    move_bar.set_parent_window(this);
}

void UIWindow::update(sf::RenderWindow &window) {
    move_bar.update(window);

    for (auto widget : widgets) {
        widget->update(window);
    }

    bg.setPosition(_rect.left, _rect.top);

    if (move_bar.is_pressed()) {
        // calculate offset
        if (!is_dragged) {
            is_dragged = true;
            sf::Vector2f pos = ise::adjusted_mouse_pos(window);
            drag_offset = {pos.x - _rect.left, pos.y - _rect.top};
        }

        // move to the closest point possible, if the window does not
        // have a bounding box it will just move straight to the mouse
        // every time
        sf::Vector2f mouse_pos = ise::adjusted_mouse_pos(window);
        move_to(mouse_pos - drag_offset);

    } else {
        is_dragged = false;
    }
}

void UIWindow::draw(sf::RenderWindow &window) {
    window.draw(bg);

    for (auto widget : widgets) {
        widget->draw(window);
    }

    move_bar.draw(window);
}

sf::Vector2<float> UIWindow::get_absolute_pos(sf::Vector2<float> pos) {
    return {_rect.left + pos.x, _rect.top + _style.movebar_height + pos.y};
}

bool UIWindow::add_widget(UIWidget &widget) {
    auto inserted = widgets.insert(&widget);
    if (!inserted.second) return false;
    widget.set_parent_window(this);
    return true;
}

bool UIWindow::remove_widget(UIWidget &widget) {
    int removed = widgets.erase(&widget);
    if (removed == 0) return false;
    widget.set_parent_window(nullptr);
    return true;
}

void UIWindow::set_visible(bool visible) { _visible = visible; }

bool UIWindow::is_visible() const { return _visible; }
bool UIWindow::is_movable() const { return _is_movable; }

bool UIWindow::contains(float x, float y) const { return contains({x, y}); }

bool UIWindow::contains(sf::Vector2<float> pos) const {
    return _rect.contains(pos);
}

bool UIWindow::process_event(const sf::Event &event) {
    bool processed = false;

    // give the event to the move bar
    if (_is_movable) {
        processed = processed || move_bar.process_event(event);
    }

    // give the event to ALL widgets
    // and check if at leas one of them processed the event
    processed = processed ||
                std::any_of(widgets.begin(), widgets.end(), [event](auto w) {
                    return w->process_event(event);
                });

    return processed;
}

void UIWindow::set_style(UIWindowStyle style) { _style = style; }

UIWindowStyle UIWindow::get_style() const { return _style; }
bool UIWindow::is_focused() const { return parent->is_focused(*this); }

void UIWindow::set_bounding_box(sf::Rect<float> bounding_box) {
    _bounding_box = bounding_box;
};

sf::Rect<float> UIWindow::get_bounding_box() const { return _bounding_box; }
bool UIWindow::move_to(sf::Vector2<float> position) {
    // check if the window has unlimited movement, if yes just move
    if (_bounding_box.width == 0 || _bounding_box.height == 0) {
        _rect.left = position.x;
        _rect.top = position.y;
        return true;
    }

    // construct the rect that would be if we moved the window
    sf::Rect<float> new_rect{position, {_rect.width, _rect.height}};
    // check if they intersect
    sf::Rect<float> out;
    bool intersecting = _bounding_box.intersects(new_rect, out);

    // check if its intersecting, then move it as much as possible
    if (intersecting) {
        // get all the relative positons(2x X and 2x Y) of all the four sides
        // of the new_rect and the intersection rect and add them up to them
        // new rects, because the recst are intersecting, at least 1 X value and
        // 1 Y value will always be 0

        // left side (from top left point)
        float rel_l = out.left - new_rect.left;
        // top side (from top left point)
        float rel_t = out.top - new_rect.top;
        // right side (bottom right point)
        float rel_r = (out.left + out.width) - (new_rect.left + new_rect.width);
        // bottom side (bottom right point)
        float rel_b = (out.top + out.height) - (new_rect.top + new_rect.height);

        // move the rect to new_rect adjusted with the relative positions

        _rect = {new_rect.left + rel_l + rel_r, new_rect.top + rel_t + rel_b,
                 new_rect.width, new_rect.height};
        return true;
    }

    return false;
}

}  // namespace ui
}  // namespace ise
