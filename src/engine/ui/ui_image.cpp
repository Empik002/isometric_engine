#include "ui_image.hpp"

#include "ui_label.hpp"
#include "ui_manager.hpp"
#include "ui_widget.hpp"
#include "ui_window.hpp"

namespace ise {
namespace ui {
void Image::set_relative_position(sf::Vector2<float> pos){
    _pos = pos;
}

void Image::draw(sf::RenderWindow& window) {
    _sprite->update();
    _sprite->set_position(get_parent_window()->get_absolute_pos(_pos));
    _sprite->draw(window);
}

}  // namespace ui
}  // namespace ise
