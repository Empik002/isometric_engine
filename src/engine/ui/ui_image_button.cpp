#include "ui_image_button.hpp"

#include "ui_manager.hpp"
#include "ui_window.hpp"

namespace ise {
namespace ui {
ImageButton::ImageButton(sf::Rect<float> rect, AnimatedSprite& sprite,
                         ImageButtonStyle style /*= {}*/)
    : UIBoxWidget(rect),
      bg(sf::Vector2f(rect.width, rect.height)),
      _sprite(&sprite),
      _style(style) {
    bg.setFillColor(_style.bg_color);
    bg.setOutlineColor(_style.outline_color);
    bg.setOutlineThickness(_style.outline_thickness);
}

void ImageButton::reset_pos() {
    bg.setPosition(
        get_parent_window()->get_absolute_pos({_rect.left, _rect.top}));

    // get center of button
    float cx = bg.getPosition().x + _rect.width / 2;
    float cy = bg.getPosition().y + _rect.height / 2;

    // get center of text
    sf::Sprite* sf_sprite = _sprite->get_sprite();
    sf::FloatRect text_rect = sf_sprite->getGlobalBounds();
    float tcx = text_rect.width / 2;
    float tcy = text_rect.height / 2;

    float lbx = tcx + sf_sprite->getLocalBounds().left;
    float lby = tcy + sf_sprite->getLocalBounds().top;
    sf_sprite->setOrigin(round({lbx, lby}));

    sf_sprite->setPosition({cx, cy});
}

void ImageButton::set_style(ImageButtonStyle style) { _style = style; }

ImageButtonStyle ImageButton::get_style() const { return _style; }

void ImageButton::draw(sf::RenderWindow& window) {
    reset_pos();

    if (is_pressed()) {
        bg.setFillColor(_style.bg_pressed_color);
    } else if (get_parent_window()->is_focused() &&
               contains(ise::adjusted_mouse_pos(window))) {
        bg.setFillColor(_style.bg_hover_color);
    } else {
        bg.setFillColor(_style.bg_color);
    }

    window.draw(bg);
    _sprite->draw(window);
}

}  // namespace ui
}  // namespace ise
