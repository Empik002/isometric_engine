#pragma once

#include "sf.hpp"
#include "ui_manager.hpp"
#include "ui_widget.hpp"

namespace ise {
namespace ui {

/**
 *  An interface that defines which functions should a window like 
 *  class implement, this is currenntly used as a base class for actuall
 *  UIWindows, and UIMultiWidget, to use the functionality, that we can
 *  set both window and UIMultiWidget as a parent window, and use the 
 *  get_absolute_pos function
 */
class UIBaseWindow{
  public:
    virtual ~UIBaseWindow(){};

    virtual sf::Vector2<float> get_absolute_pos(sf::Vector2<float> pos) = 0;
    virtual bool add_widget(UIWidget& widget) = 0;
    virtual bool is_focused() const = 0;
};

}  // namespace ui
}  // namespace ise
