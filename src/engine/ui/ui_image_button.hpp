#pragma once

#include <SFML/Graphics/Color.hpp>
#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/RenderWindow.hpp>

#include "animated_sprite.hpp"
#include "sf.hpp"
#include "ui_box_widget.hpp"
#include "ui_manager.hpp"
#include "utilities.hpp"
#include "ui_styles.hpp"

namespace ise {
namespace ui {


// this copies an animatedprite(s) you provide it
class ImageButton : public UIBoxWidget {
    sf::RectangleShape bg;
    ise::AnimatedSprite* _sprite;
    ImageButtonStyle _style;

    void reset_pos();

  public:
    virtual ~ImageButton(){}

    ImageButton(sf::Rect<float> rect, AnimatedSprite& sprite, 
            ImageButtonStyle style = {});

    void set_style(ImageButtonStyle style);
    ImageButtonStyle get_style() const;

    void draw(sf::RenderWindow& window) override;
};


}  // namespace ui
}  // namespace ise
