#pragma once

#include <SFML/Graphics/Color.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/System/Vector2.hpp>

#include "sf.hpp"
#include "ui_manager.hpp"
#include "ui_widget.hpp"
#include "ui_window.hpp"
#include "utilities.hpp"

namespace ise {
namespace ui {

class Label : public UIWidget {
    sf::Vector2<float> _pos;
    sf::Text* _text;

   public:
    virtual ~Label() {}
    Label(sf::Vector2<float> relative_pos, sf::Text& text)
        : _pos(relative_pos), _text(&text) {}

    void set_relative_position(sf::Vector2<float> pos) override;
    void draw(sf::RenderWindow& window) override;
};

}  // namespace ui
}  // namespace ise
