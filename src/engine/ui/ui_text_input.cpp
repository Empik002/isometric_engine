#include "ui_text_input.hpp"

#include <SFML/Window/Event.hpp>

#include "ui_box_widget.hpp"

namespace ise {
namespace ui {


//TODO: Finis the text input, add deleting support, add scrolling support,
// imporve the visuals

TextInput::TextInput(sf::Rect<float> rect, std::string& text, int char_limit,
                     TextInputStyle style /*= {}*/)
    : UIBoxWidget(rect), _out(&text), _char_limit(char_limit), _style(style) {
        _text.setFont(*style.font);
    }

bool TextInput::is_focused() const { return focused; }

bool TextInput::add_character(const sf::Event& event) {
    // check if the widget is focused AND the window is focused otherwise
    // do not add the character
    if (!is_focused() || !get_parent_window()->is_focused()) {
        return false;
    }


    // add the character if we didnt exceed the limit
    if (_char_limit == 0 || _out->size() < _char_limit) {
        *_out += event.text.unicode;
        // set the updated variable because we added a character
        updated = true;
        return true;
    }

    return false;
}

bool TextInput::process_event(const sf::Event& event) {
    
    // pass the event to base class
    bool procesed = UIBoxWidget::process_event(event);
    if (procesed) {
        // do not continue if it was procesed
        // by the UIBoxWidget meaning it was clicked
        focused = true;
        return true;
    }

    switch (event.type) {
        // handle clicking outside of the widget, anytime you click outside
        // the widget set the focused variable to false.
        // Clicking on the widget is handled above
        case sf::Event::MouseButtonPressed:  // fall through
        case sf::Event::MouseButtonReleased:
            focused = false;
            return false;

        //case sf::Event::KeyPressed:
        case sf::Event::TextEntered:
            return add_character(event);

        default:
            return false;
    }
}

void TextInput::update(sf::RenderWindow& window) {
    if (updated) {
        // rerender the text
        _text.setString(*_out);

        updated = false;  // reset
    }

    // check if the window was unfocused, if yes unfocus the widget
    if (!get_parent_window()->is_focused()) {
        focused = false;
    }
}

void TextInput::reset_pos() {
    //TODO: this exact code but with x axis as well is in button, prob
    //elsewhere too, seúarate it into a function
    bg.setPosition(
        get_parent_window()->get_absolute_pos({_rect.left, _rect.top}));

    // get middle on y axis
    float cy = bg.getPosition().y + _rect.height / 2;

    // get middle on y axis of text
    sf::FloatRect text_rect = _text.getGlobalBounds();
    float tcy = text_rect.height / 2;

    float lby = tcy + _text.getLocalBounds().top;
    _text.setOrigin(round({0, lby}));

    // set text position
    _text.setPosition({bg.getPosition().x, cy});
}

void TextInput::draw(sf::RenderWindow &window) {
    reset_pos();
    // set bg size
    bg.setSize({_rect.width, _rect.height});

    if (is_pressed()) {
        bg.setFillColor(_style.bg_pressed_color);
    } else if (get_parent_window()->is_focused() &&
               contains(ise::adjusted_mouse_pos(window))) {
        bg.setFillColor(_style.bg_hover_color);
    } else {
        bg.setFillColor(_style.bg_color);
    }


    window.draw(bg);
    window.draw(_text);
}


}  // namespace ui
}  // namespace ise
