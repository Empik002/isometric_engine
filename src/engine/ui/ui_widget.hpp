#pragma once

#include "sf.hpp"
#include "ui_manager.hpp"

namespace ise {
namespace ui {

/* ABSTRACT base class for ui widgets */
class UIWidget {
  protected:
    UIBaseWindow* _parent_window = nullptr;
   public:
    
    UIWidget() = default;
    virtual ~UIWidget() {}

    virtual UIBaseWindow* get_parent_window() const {return _parent_window;}

    /**
     * @brief Set the parent window, it saves a pointer.
     * in case of MultiWidgets this function will be overriden
     *
     * @param UIWindow& parent_window
     */
    virtual void set_parent_window(UIBaseWindow* parent_window){
        _parent_window = parent_window; 
    }

    /**
     * @brief Override fo the set_parent_window with pointer
     * just for compatibility with the rest of the api, the pointer one
     * is used by the internal functions
     *
     * @param parent_window
     */
    virtual void set_parent_window(UIBaseWindow& parent_window){
        set_parent_window(&parent_window);
    }

    /**
     * @brief Set the positon to the rect you provide when creating new
     * widget
     *
     * @param pos
     */
    virtual void set_relative_position(sf::Vector2<float> pos) = 0;

    /**
     * @brief Handle provided event 
     *
     * @param sf::Event&
     *
     * @return bool returns if the event was processed
     */
    virtual bool process_event(const sf::Event&) { return false; };
    virtual void update(sf::RenderWindow&){}
    virtual void draw(sf::RenderWindow&){}

};

}  // namespace ui
}  // namespace ise
