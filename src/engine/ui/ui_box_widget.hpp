#pragma once
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/System/Vector2.hpp>
#include <SFML/Window/Event.hpp>

#include "ui_widget.hpp"

namespace ise {
namespace ui {

/* ABSTRACT base class for ui widgets */
class UIBoxWidget : public UIWidget {
   protected:
    sf::Rect<float> _rect;

    // this function sets the appropriate variables if it was clicked
    // or is presed and vice versa, should be called at the start of
    // child class process_event
    // IMPORTANT!!!!:
    // update_click refreshes the value of was presed, if mouse is not being
    // hovered over it!! so that moving the mouse away while holding
    // down the button, releasing, then clicking  and returning
    // back to the widget, doesnt fire the clicked event
    bool process_click_event(sf::Event event);

   private:
    bool was_pressed = false;
    bool clicked = false;

   public:
    UIBoxWidget(sf::Rect<float> rect) : _rect(rect) {}
    virtual ~UIBoxWidget() {}
    
    virtual void set_relative_position(sf::Vector2<float> pos) override;

    void update(sf::RenderWindow& window) override;
    bool process_event(const sf::Event& event) override;

    bool contains(float x, float y);
    bool contains(sf::Vector2<float> pos);

    bool is_clicked() const;
    bool is_pressed() const;
};

}  // namespace ui
}  // namespace ise
