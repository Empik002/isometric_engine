#pragma once

#include <SFML/Graphics/Color.hpp>
#include <SFML/Graphics/Font.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/View.hpp>
#include <SFML/Window/Event.hpp>
#include <algorithm>
#include <list>
#include <ranges>
#include <set>

#include "sf.hpp"

/*
 * IMPORTANT!!!!!!!!!!!!!!!!!
 * UI does NOT hold onto any object, everything is just a pointer to
 * objects u created elsewhere, you are repsonsible for those objects
 * living untill the UI manager is destroyed
 *
 * you HAVE to call update before you query any state (for example like
 * is clicked)
 *
 * */
namespace ise {
namespace ui {

// Forward declare all the UI classes
class UIConfig;
class UI;
class UIBaseWindow;
class UIWindow;
class UIWidget;     // base class for all widgets
class UIBoxWidget;  // base class for all rectangular widgets



// Declare the ui manager class
class UI {
    UIWindow* focused_window = nullptr;
    std::list<UIWindow*> windows;

    std::set<sf::Keyboard::Key> pressed_keys;

    // This will house the original view from the render window, until
    // you call the end view, then it will be swapped back to the view
    sf::View original_view;

    // this houses the view for the ui
    sf::View ui_view;


    bool handle_key_event(const sf::Event& event);
    bool handle_mouse_event(const sf::Event& event);
    bool handle_mouse_left_pressed(const sf::Event& event);
    bool handle_mouse_left_released(const sf::Event& event);
    bool handle_mouse_move(const sf::Event& event);

    /**
     * @brief sets the view that is supposed to be used by the ui
     *
     * @param const sf::view& the view, it will be coppied
     */
    void set_ui_view(const sf::View& view);

    /**
     * @brief Sets the UIs view, you MUST call end_view after you are
     * finished with the view!!!!!!!!
     * @param sf::RenderWindow& window to which to set the view
     */
    void begin_view(sf::RenderWindow& window);

    /**
     * @brief unsets the UIs view;
     * @param sf::RenderWindow& window, sets the original view back
     * to the window
     */
    void end_view(sf::RenderWindow& window);

   public:
    UI() = delete; // we nneed the render window
    explicit UI(const sf::RenderWindow& window);  // constructor

    void add_window(UIWindow& window);

    // O(n)
    void remove_window(UIWindow& window);
    // Invalidates the iterators to the window; (calls erase)
    void focus_window(UIWindow& window);
    void unfocus_window();
    
    // Static variable for default font all widget styles 
    // set their font variable to this by default
    // you need to set it at the beginning if you plan on using
    // it Otherwise all the text will be empty
    static sf::Font default_font;
    static void set_default_font(const sf::Font& font);
    static sf::Font* get_default_font();

    // this function returns true if it did something with the event
    // meaning that if you desire you can skip said event and not handle it
    // again. For example you can prevent your program from clicking through
    // ui windows as this will always return true if a ui window is clicked!
    bool process_event(const sf::Event& event, sf::RenderWindow& window);

    /**
     * @brief Return if the given window is currently focused
     *
     * @param const UIWindow& window
     *
     * @return bool
     */
    bool is_focused(const UIWindow& window) const;

    // returns if the ui manager has a window currently focused;
    bool has_focus() const;

    void update(sf::RenderWindow& render_window);
    void draw(sf::RenderWindow& render_window);
};


}  // namespace ui
}  // namespace ise
