#pragma once

#include <SFML/System/Vector2.hpp>
#include <SFML/Window/Event.hpp>
#include <memory>
#include <set>

#include "log.hpp"
#include "sf.hpp"
#include "ui_base_window.hpp"
#include "ui_button.hpp"
#include "ui_manager.hpp"
#include "ui_styles.hpp"
#include "ui_widget.hpp"

namespace ise {
namespace ui {

class UIWindow : public UIBaseWindow {
    sf::Rect<float> _rect;
    sf::RectangleShape bg;
    bool _is_movable = true;
    UIWindowStyle _style;

    std::set<UIWidget*> widgets;

    bool _visible = true;

    bool is_dragged = false;
    sf::Vector2<float> drag_offset;

    /**
     * @brief The bounding box where the window is allowed to move
     * if the width or height (or both) are zero, it has unrestricted movement.
     */
    sf::Rect<float> _bounding_box{0, 0, 0, 0};

    sf::Text default_button_text;
    ise::ui::Button move_bar{{0, 0, 0, 0}, default_button_text};

   public:
    UI* parent = nullptr;

    UIWindow() = delete;
    UIWindow(sf::Rect<float> rect, sf::Text* name = nullptr,
             bool is_movable = true, UIWindowStyle style = {});

    sf::Vector2<float> get_absolute_pos(sf::Vector2<float> pos) override;

    void set_style(UIWindowStyle style);
    UIWindowStyle get_style() const;

    void set_bounding_box(sf::Rect<float> bounding_box);
    sf::Rect<float> get_bounding_box() const;

    bool add_widget(UIWidget& widget) override;
    bool remove_widget(UIWidget& widget);

    bool process_event(const sf::Event& event);
    void unfocus_widget();

    bool is_focused() const override;
    bool is_movable() const;
    bool is_visible() const;
    void set_visible(bool visible);

    /**
     * @brief Function that moves the window to the set position but takes
     *        into account the bounding box. If the position is not in the
     *        bounding box it will not move the window. If you want the
     *        window to be movable anywhere the bounding_box needs to have
     *        its width and height set to zero.
     *
     * @param position
     *
     * @return true if the window was moved at all, (does not mean it
     * was moved straight to that point, TODO: probably change this)
     */
    bool move_to(sf::Vector2<float> position);

    // return if it contains the position, eg if is hovered
    bool contains(float x, float y) const;
    bool contains(sf::Vector2<float> pos) const;

    void update(sf::RenderWindow& window);
    void draw(sf::RenderWindow& window);
};

}  // namespace ui
}  // namespace ise
