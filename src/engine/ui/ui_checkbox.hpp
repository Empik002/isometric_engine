#pragma once

#include <SFML/Graphics/Color.hpp>
#include <SFML/Graphics/RenderWindow.hpp>

#include "sf.hpp"
#include "ui_manager.hpp"
#include "ui_widget.hpp"
#include "ui_window.hpp"
#include "utilities.hpp"
#include "ui_styles.hpp"

namespace ise {
namespace ui {


class CheckBox : public UIBoxWidget {
    sf::RectangleShape bg;
    sf::RectangleShape check;
    bool* _checked;
    CheckBoxStyle _style;

    void reset_pos();

   public:
    virtual ~CheckBox() {}

    CheckBox(sf::Rect<float> rect, bool& value, CheckBoxStyle style = {});

    bool is_checked() const;
    
    void set_style(CheckBoxStyle style);
    CheckBoxStyle get_style() const;

    void update(sf::RenderWindow& window) override;
    void draw(sf::RenderWindow& window) override;
};

}  // namespace ui
}  // namespace ise
