#pragma once

#include "sf.hpp"


namespace ise{
namespace debug {
    
    void draw_pixel(sf::RenderWindow& window,int x, int y);
    
}// namespace ise::debug
} // namespace ise
