#include "tilemap.hpp"

#include <SFML/System/Vector2.hpp>

namespace ise {

void Tile::set_pos(float x, float y) { _sprite.set_position(x, y); }

void Tile::set_pos(sf::Vector2<float> pos) { _sprite.set_position(pos); }

sf::Vector2<float> Tile::get_pos() const { return _sprite.get_position(); }

AnimatedSprite &Tile::get_sprite() { return _sprite; }

void Tile::set_y_offset(float off) {
    sf::Vector2f position = get_pos();
    position.y -= _y_offset;
    position.y += off;
    _y_offset = off;
}

float Tile::get_y_offset() const { return _y_offset; }

void Tile::set_top_offset(float off) { _top_offset = off; }

float Tile::get_top_offset() const { return _top_offset; }

void Tile::set_fps(float fps) { _sprite.set_fps(fps); }

void Tile::update() { _sprite.update(); }

void Tile::draw(sf::RenderTarget &window) const { _sprite.draw(window); }


/*
 *
 *  Tilemap
 *
 */

sf::Vector2<int> Tilemap::corner_position(int x_index, int y_index,
                                          int tile_top_offset) const {
    return {(x_index * _tile_width) + _origin_offset.x,
            (y_index * _tile_height) + _origin_offset.y};
}

sf::Vector2<int> Tilemap::indices(int x_position, int y_position) const {
    return {x_position / _tile_width, y_position / _tile_height};
}

void Tilemap::set_tile(int x_index, int y_index, Tile &tile) {
    erase_tile(x_index, y_index);

    sf::Vector2<int> pos =
        corner_position(x_index, y_index, tile.get_top_offset());

    tile.set_pos(pos.x, pos.y);
    tiles.insert(std::make_pair(std::make_pair(x_index, y_index), tile));
}

void Tilemap::erase_tile(int x_index, int y_index) {
    auto it = tiles.find(std::make_pair(x_index, y_index));
    if (it != tiles.end()) tiles.erase(it);
}


//TODO: check if this is correct
ise::Tile* Tilemap::get_tile(int x_index, int y_index){
    return &(tiles.at(std::make_pair(x_index, y_index))); 
}

void Tilemap::draw(sf::RenderTarget &window) const {
    // as it is in an ordered map with x_y it is sorted in a way that it 
    // renders properly
    for (auto &[_ , tile] : tiles) {
        tile.draw(window);
    }
}

void Tilemap::draw(sf::RenderTarget &window, 
        std::map<std::pair<int,int>, Tile>& override_tiles) const{
    auto left = tiles.begin();
    auto right = override_tiles.begin();
    

    // merge 
    while ((left != tiles.end()) && (right != override_tiles.end())){
       std::pair<int, int> left_key = left->first;
       std::pair<int, int> right_key = right->first;

       const Tile& left_tile = left->second;
       const Tile& right_tile = right->second;

       if (left_key < right_key){
           left_tile.draw(window);
           ++left;
       } else if (left_key > right_key){
           right_tile.draw(window);
           ++right;
       } else{
           // if equal render the right only and move both to skip the non 
           // fake tile
           right_tile.draw(window);
           ++left;
           ++right;
       }
    } 

    // finish drawing left
    ise::Tilemap::draw(window, left, tiles.end());
   
    // or finidh drawing right
    ise::Tilemap::draw(window, right, override_tiles.end());
}

void Tilemap::draw(sf::RenderTarget &window, 
        std::map<std::pair<int, int>, Tile>::const_iterator begin,
        std::map<std::pair<int, int>, Tile>::const_iterator end){
   for(;begin != end; ++begin){
       const Tile& tile = begin->second;
       tile.draw(window);
   }
}

void Tilemap::update() {
    for (auto &[_, tile] : tiles) {
        tile.update();
    }
}

int Tilemap::get_tile_height() const { return _tile_height; }

int Tilemap::get_tile_width() const { return _tile_width; }

std::map<std::pair<int, int>, Tile>::const_iterator Tilemap::tiles_begin()
    const {
    std::map<std::pair<int, int>, Tile>::const_iterator it = tiles.begin();
    return it;
}

std::map<std::pair<int, int>, Tile>::const_iterator Tilemap::tiles_end() const {
    std::map<std::pair<int, int>, Tile>::const_iterator it = tiles.end();
    return it;
}

const std::string Tilemap::get_type() const{
    return std::string("Standard");
}
/*
 *
 * IsoTilemap
 *
 */

sf::Vector2<int> IsoTilemap::corner_position(
        int x_index, int y_index, int tile_top_offset) const{

    int x = x_index * 0.5 * _tile_width + y_index * (-0.5) * _tile_width;
    int y = x_index * 0.25 * _tile_height + y_index * 0.25 * _tile_height -
           tile_top_offset;

    return {x,y};
}

sf::Vector2<int> IsoTilemap::indices(int x_position, int y_position) const{
    int x_index = std::round(x_position * 0.25 * _tile_height / determinant() +
        y_position * 0.5 * _tile_width / determinant()) - 1;

    int y_index = std::round(x_position * (-0.25) * _tile_height / determinant() +
                      y_position * 0.5 * _tile_width / determinant());
    
    return {x_index, y_index};
}

float IsoTilemap::determinant() const {
    return (0.5 * _tile_width) * (0.25 * _tile_height) +
           (0.5 * _tile_width) * (0.25 * _tile_height);
}

const std::string IsoTilemap::get_type() const{
    return std::string("Isometric");
}

}  // namespace ise
