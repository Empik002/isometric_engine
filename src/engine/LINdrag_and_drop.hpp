#pragma once

#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Window/WindowHandle.hpp>

#include <X11/X.h>
#include <X11/Xlib.h>
#include <sys/types.h>

#include <array>
#include <iostream>
#include <memory>
#include <string>
#include <vector>

#include "log.hpp"

namespace ise {

#define XDND_PROTOCOL_VERSION 5

using XWindow = Window;

struct XDNDAtoms {
    Atom XdndAware = BadAtom;
    Atom XA_ATOM = BadAtom;
    Atom XdndEnter = BadAtom;
    Atom XdndPosition = BadAtom;
    Atom XdndActionCopy = BadAtom;
    Atom XdndLeave = BadAtom;
    Atom XdndStatus = BadAtom;
    Atom XdndDrop = BadAtom;
    Atom XdndSelection = BadAtom;
    Atom XDND_DATA = BadAtom;
    Atom XdndTypeList = BadAtom;
    Atom XdndFinished = BadAtom;
    std::array<Atom, 6> typesWeAccept = {BadAtom};
};

struct XDNDStateMachine {
    Display* disp = nullptr;
    Window wind = BadWindow;
    bool xdndExchangeStarted = false;
    bool xdndPositionReceived = false;
    bool xdndStatusSent = false;
    Time xdndDropTimestamp = 0;
    bool amISource = false;
    Window otherWindow = BadWindow;
    Atom proposedAction = BadAtom;
    Atom proposedType = BadAtom;
    XDNDAtoms atoms = {};
};

// Base-from-Member
// https://en.wikibooks.org/wiki/More_C%2B%2B_Idioms/Base-from-Member
class XDNDRenderWindow_pbase {
   protected:
    XWindow _window_handle = 0;
    explicit XDNDRenderWindow_pbase(sf::VideoMode mode, const sf::String& title,
                                    sf::Uint32 style = sf::Style::Default);
};

class XDNDRenderWindow : protected XDNDRenderWindow_pbase,
                         public sf::RenderWindow {
   public:
    // constructor (pretty uggly but ehh)
    explicit XDNDRenderWindow(
        sf::VideoMode mode, const sf::String& title,
        sf::Uint32 style = sf::Style::Default,
        const sf::ContextSettings& settings = sf::ContextSettings())
        : XDNDRenderWindow_pbase(mode, title, style),
          sf::RenderWindow(_window_handle, settings) {
        std::cout << _window_handle << std::endl;
    }
};

Display* openDisplay(bool close = false);

void sendXdndFinished(XDNDStateMachine& xdndState);
void sendXdndStatus(XDNDStateMachine& xdndState, Atom action);
bool doWeAcceptAtom(Atom a, XDNDStateMachine& xdndState);
Atom getSupportedType(XDNDStateMachine& xdndState);
std::vector<std::string> getCopiedData(XDNDStateMachine& xdndState);
void XDNDinit_atoms(Display* disp, XDNDAtoms& atoms);
XDNDStateMachine XDNDinit(XDNDRenderWindow& window);
void XDNDexit(XDNDStateMachine& xdndState);
std::vector<std::string> XDNDupdate(XDNDStateMachine& xdndState);
void XDNDClearState(XDNDStateMachine& xdndState);

}  // namespace ise

