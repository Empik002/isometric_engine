#include "string_util.hpp"

#include <iostream>
#include <string>

// this functions excepts only correct values, otherwise it will
// throw an exception
// Parameters: string with two signed integers separated by a comma, no spaces
// example: "2,1" or "-3,-5" or "36,-10000"
sf::Vector2<int> split_str_to_x_y(const std::string& value) {
    std::size_t comma_index = value.find(",");

    if (comma_index == std::string::npos) {
        throw std::invalid_argument("Value '" + value +
                                    "' does not contain a comma");
    }
    int x = std::stoi(std::string(value, 0, comma_index));
    int y = std::stoi(std::string(value, comma_index + 1));

    return {x, y};
}

std::string int_pair_to_brackets(const std::pair<int, int>& value) {
    std::string out =
        std::to_string(value.first) + "," + std::to_string(value.second);

    return out;
}
