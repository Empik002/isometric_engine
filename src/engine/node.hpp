#pragma once

#include "sf.hpp"

/*
 * This file will be expanded in the future.
 * */

namespace ise {

/* ABSTRACT base class for most of the engine */
class Node {
  public:
    Node() = default;
    virtual ~Node() {} 
    
    std::string name;
    // y level for future sorting
    int y_level = 0;

    virtual void update() {}
    virtual void draw(sf::RenderTarget &) const {}
};

} // namespace ise
