#pragma once

#include <string>
#include "sf.hpp"

namespace ise {

/* ABSTRACT base class for Resources */
class Resource {
  public:
    Resource() = default;
    virtual ~Resource(){} //= 0;
    std::string name;
};

} // namespace ise
