#pragma once

#include "animated_sprite.hpp"
#include "texture.hpp"
#include "tilemap.hpp"

enum class AssetType {
    ANIMATED_SPRITE,
    TEXTURE,
    TILE,
    TILEMAP,
    ISOMETRIC_TILEMAP
};
