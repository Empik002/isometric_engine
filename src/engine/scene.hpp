#pragma once
#include <map>
#include <vector>
#include <memory>
#include "animated_sprite.hpp"
#include "node.hpp"
#include "resource.hpp"
#include "assets.hpp"


namespace ise{

/*ABSTRACT class for scenes*/
class Scene {
  public:
    std::map<std::string, std::unique_ptr<Texture>> textures;
    std::map<std::string, std::unique_ptr<AnimatedSprite>> sprites;
    std::map<std::string, std::unique_ptr<Tile>> tiles;
    std::map<std::string, std::unique_ptr<Tilemap>> tilemaps;

    // this will be later used to update selected nodes
    std::vector<Node*> nodes;

    ~Scene() {}
    void update();
    void clear();
    
    //friend class ResLoader;
};

} // namespace ise
