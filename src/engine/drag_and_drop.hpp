/*
 * This is a file that creates a unified API for handling drag and drop
 * on multiple platforms
 */
#pragma once
#include "sf.hpp"


#if defined LIN
#include "LINdrag_and_drop.hpp"

namespace ise{

// Linux section
using DND_state = XDNDStateMachine;
using DNDRenderWindow = XDNDRenderWindow;

DND_state DND_init(DNDRenderWindow& window);
std::vector<std::string> DND_update(DND_state& state);
void DND_exit(DND_state& state);

} // namespace ise


#elif defined WIN
#include "WINdrag_and_drop.hpp"


namespace ise{

using DND_state = WINDNDState;
using DNDRenderWindow = sf::RenderWindow;

DND_state DND_init(DNDRenderWindow& window);
std::vector<std::string> DND_update(DND_state& state);
void DND_exit(DND_state& state);
} // namespace ise

#else
// default option just to make the linter shut up...
#include <vector>
#include <string>
namespace ise{

struct DND_state{unsigned int wind = 0;};
using DNDRenderWindow = sf::RenderWindow;


DND_state DND_init(DNDRenderWindow& window);
std::vector<std::string> DND_update(DND_state& state);
void DND_exit(DND_state& state);
}
#endif
