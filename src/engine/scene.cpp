#include "scene.hpp"

namespace ise{
void Scene::clear(){
    textures.clear();
    sprites.clear();
    tiles.clear();
    tilemaps.clear();
    nodes.clear();
}
} // namespace ise
