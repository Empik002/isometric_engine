#pragma once
#include <SFML/Graphics/Texture.hpp>

#include "resource.hpp"

namespace ise {
class Texture : public Resource {
    /*Class that wraps normal sf::texture into a Node,
     * mostly for adding more attributes that will fit with
     * the engine structure.
     *
     * it contains overload for conversion
     *      from ise::Texture to sf::Texture*/
    sf::Texture texture;

   public:
    std::string file_name;

    Texture() = default;
    Texture(std::string path);
    
    bool loadFromFile(std::string&& path);

    operator sf::Texture() const { return texture; }
    operator sf::Texture&() { return texture; }

    sf::Texture copy_sf_texture() const { return texture; }
    sf::Texture& get_texture() { return texture; }
};
} /* namespace ise */
