#include "res_loader.hpp"

#include <filesystem>
#include <memory>
#include <utility>

#include "animated_sprite.hpp"

using json = nlohmann::json;

namespace ise {

// see the docs/json_formats files for pretty versions
const std::string ResLoader::DEFAULT_PROJECT_CONFIG_STRING =
    "{\"BG_COLOR\": {\"R\": 0.112227806,\"G\": 0.176470588,\"B\": "
    "0.356862745},\"TOP_OFFSET\": 24}";
const std::string ResLoader::DEFAULT_CONFIG_STRING =
    "{\"PROJECT_HISTORY\": []}";

std::filesystem::path ResLoader::assets_path;

const std::filesystem::path ResLoader::ASSETS_JSON_STUB{"json"};
const std::filesystem::path ResLoader::ASSETS_IMG_STUB{"img"};
const std::filesystem::path ResLoader::ASSETS_ANIMATED_STUB{"img/animated"};
const std::filesystem::path ResLoader::ASSETS_FONT_STUB{"fonts"};

json ResLoader::config = json::parse(DEFAULT_CONFIG_STRING);
json ResLoader::project_config = json::parse(DEFAULT_PROJECT_CONFIG_STRING);

std::pair<bool, nlohmann::json> ResLoader::load_json(
    std::filesystem::path path) {
    json data;
    if (!std::filesystem::is_regular_file(path)) {
        return std::make_pair(false, data);
    }

    std::ifstream f(path);

    try {
        data = json::parse(f);
    } catch (const json::parse_error&) {
        return std::make_pair(false, data);
    }

    return std::make_pair(true, data);
}

bool ResLoader::save_json(nlohmann::json data, std::filesystem::path path) {
    LOG(2, "Saving to path:", path.string().c_str());
    std::ofstream json_file(path, std::ios_base::trunc | std::ios_base::out);
    if (!json_file) {
        LOG(1, "> Couldn't open the file");
        return false;
    }

    json_file << data;
    return true;
}

// --------------- SINGLE LOAD
bool ResLoader::load_texture(ise::Scene& scene, std::filesystem::path path) {
    std::pair<bool, json> data = load_json(path);
    if (!data.first) return false;

    return load_texture(scene, data.second);
}

bool ResLoader::load_anim_sprite(ise::Scene& scene,
                                 std::filesystem::path path) {
    std::pair<bool, json> data = load_json(path);
    if (!data.first) return false;
    return load_anim_sprite(scene, data.second);
}

bool ResLoader::load_tile(ise::Scene& scene, std::filesystem::path path) {
    std::pair<bool, json> data = load_json(path);
    if (!data.first) return false;
    return load_tile(scene, data.second);
}

bool ResLoader::load_tilemap(ise::Scene& scene, std::filesystem::path path) {
    std::pair<bool, json> data = load_json(path);
    if (!data.first) return false;
    return load_tilemap(scene, data.second);
}

bool ResLoader::load_texture(ise::Scene& scene, nlohmann::json data) {
    LOG(2, "Loading texture: ", std::string(data["FileName"]).c_str());

    // allocate on heap
    std::unique_ptr<ise::Texture> texture_p = std::make_unique<ise::Texture>();
    if (!texture_p->loadFromFile("assets/img/" +
                                 std::string(data["FileName"]))) {
        LOG(1, "Missing texture");  // log this //texture = missing;
    }

    texture_p->name = data["Name"];

    LOG(1, "Loaded Texture");

    // transfer ownership
    scene.textures.insert({data["Name"], std::move(texture_p)});
    return true;
};

bool ResLoader::load_texture_from_png(ise::Scene& scene,
                                      std::filesystem::path path) {
    // allocate on heap
    std::unique_ptr<ise::Texture> texture_p = std::make_unique<ise::Texture>();

    if (!texture_p->loadFromFile(path.string())) {
        LOG(1, "Missing texture");
    }  // texture = missing;
    texture_p->name = path.stem().string();

    // handoff the ownership to the scene
    // inserst a std::pair of name and the pointer
    scene.textures.insert({texture_p->name, std::move(texture_p)});
    return true;
}

bool ResLoader::load_anim_sprite(ise::Scene& scene, nlohmann::json data) {
    LOG(2, "Started loading sprite: ", std::string(data["Name"]).c_str());
    if (!scene.textures.contains(data["TextureName"])) {
        LOG(1, (((assets_path / ASSETS_JSON_STUB /
                  std::string(data["TextureName"]))
                     .concat(".json")))
                   .c_str());

        bool loaded = load_texture(scene, (assets_path / ASSETS_JSON_STUB /
                                           std::string(data["TextureName"]))
                                              .concat(".json"));
        if (!loaded) return false;
    }

    // alocate on heap
    std::unique_ptr<ise::AnimatedSprite> sprite_p =
        std::make_unique<ise::AnimatedSprite>(
            // intentionaly dereference to copy to animated sprite
            *scene.textures.at(data["TextureName"]),
            data["Attributes"]["Width"], data["Attributes"]["Height"],
            data["Attributes"]["Fps"]);

    sprite_p->name = data["Name"];
    // transfer
    scene.sprites.insert({data["Name"], std::move(sprite_p)});
    return true;
}

bool ResLoader::load_tile(ise::Scene& scene, nlohmann::json data) {
    LOG(2, "Started loading tile: ", std::string(data["Name"]).c_str());
    if (!scene.sprites.contains(data["SpriteName"])) {
        bool loaded = load_anim_sprite(scene, (assets_path / ASSETS_JSON_STUB /
                                               std::string(data["SpriteName"]))
                                                  .concat(".json"));
        if (!loaded) return false;
    }

    // alocate on heap
    std::unique_ptr<ise::Tile> tile_p = std::make_unique<ise::Tile>(
        // intentionaly dereference to copy
        *scene.sprites.at(data["SpriteName"]), data["Attributes"]["TopOffset"]);

    tile_p->set_y_offset(data["Attributes"]["YOffset"]);
    tile_p->name = data["Name"];
    // transfer
    scene.tiles.insert({data["Name"], std::move(tile_p)});
    return true;
}

bool ResLoader::load_tilemap(ise::Scene& scene, nlohmann::json data) {
    LOG(2, "Started loading Tilemap: ", std::string(data["Name"]).c_str());
    // get tilemap Type
    std::string type = data["TilemapType"];

    // alocate on heap
    //  create empty pointer
    std::unique_ptr<ise::Tilemap> tilemap_p;

    if (type == "Standard") {
        tilemap_p = std::make_unique<ise::Tilemap>(
            data["Attributes"]["TileWidth"], data["Attributes"]["TileHeight"],
            sf::Vector2f(data["Attributes"]["OriginOffset"][0],
                         data["Attributes"]["OriginOffset"][1]));

    } else if (type == "Isometric") {
        tilemap_p = std::make_unique<ise::IsoTilemap>(
            data["Attributes"]["TileWidth"], data["Attributes"]["TileHeight"],
            sf::Vector2f(data["Attributes"]["OriginOffset"][0],
                         data["Attributes"]["OriginOffset"][1]));

    } else {
        LOG(2, "Wrong type of tilemap: ", type.c_str());
        return false;
    }

    // load the data
    auto it = (data["Attributes"]["TileData"]).begin();
    const auto it_end = (data["Attributes"]["TileData"]).end();
    // traverse the TileData object and set appropriate tiles from tile map
    for (; it != it_end; it++) {
        std::string position = it.key();
        std::string tile_name = it.value();

        // skip if tile was not found, probbably a missing tile,
        // or incorrect tile in tilemap json
        if (!scene.tiles.contains(tile_name)) {
            LOG(1, ">>Error: Could not find the tile in loaded tiles");
            continue;
        }

        sf::Vector2<int> indices = split_str_to_x_y(position);
        // intentionaly dereference to copy
        tilemap_p->set_tile(indices.x, indices.y, *scene.tiles.at(tile_name));
    }

    tilemap_p->name = data["Name"];
    // transfer
    scene.tilemaps.insert({data["Name"], std::move(tilemap_p)});
    return true;
}

// ------------------ BULK LOAD

void ResLoader::load_textures_from_pngs(ise::Scene& scene,
                                        std::filesystem::path img_dir_path) {
    std::filesystem::directory_iterator dir(img_dir_path);
    for (const auto& entry : dir) {
        if (!entry.is_regular_file()) continue;
        if (entry.path().extension() != ".png") continue;
        LOG(2, "Loading texture from png: ", entry.path().c_str());
        load_texture_from_png(scene, entry.path());
    }
}

void ResLoader::load_textures_from_pngs(ise::Scene& scene) {
    load_textures_from_pngs(scene, assets_path / ASSETS_IMG_STUB);
    load_textures_from_pngs(scene, assets_path / ASSETS_ANIMATED_STUB);
}

void ResLoader::load_textures(ise::Scene& scene) {
    std::filesystem::directory_iterator jsondir(assets_path / ASSETS_JSON_STUB);
    for (const auto& entry : jsondir) {
        // basic checks
        if (!entry.is_regular_file()) continue;
        if (entry.path().extension() != ".json") continue;

        // load and process the json
        std::ifstream f(entry.path());
        json data = json::parse(f);

        if (data["Type"] != "Asset") continue;

        if (data["AssetType"] != "Texture") continue;

        // create and store the texture
        load_texture(scene, data);
    }
}

void ResLoader::load_anim_sprites(ise::Scene& scene) {
    std::filesystem::directory_iterator jsondir(assets_path / ASSETS_JSON_STUB);
    for (const auto& entry : jsondir) {
        // basic checks
        if (!entry.is_regular_file()) continue;
        if (entry.path().extension() != ".json") continue;

        // load and process the json
        std::ifstream f(entry.path());
        json data = json::parse(f);

        if (data["Type"] != "Asset") continue;

        if (data["AssetType"] != "AnimatedSprite") continue;

        load_anim_sprite(scene, data);
    }
}

void ResLoader::load_tiles(ise::Scene& scene) {
    std::filesystem::directory_iterator jsondir(assets_path / ASSETS_JSON_STUB);
    for (const auto& entry : jsondir) {
        // basic checks
        if (!entry.is_regular_file()) continue;
        if (entry.path().extension() != ".json") continue;

        // load and process the json
        std::ifstream f(entry.path());
        json data = json::parse(f);

        if (data["Type"] != "Asset") continue;
        if (data["AssetType"] != "Tile") continue;

        load_tile(scene, data);
    }
}

void ResLoader::load_tilemaps(ise::Scene& scene) {
    std::filesystem::directory_iterator jsondir(assets_path / ASSETS_JSON_STUB);
    for (const auto& entry : jsondir) {
        // basic checks
        if (!entry.is_regular_file()) continue;
        if (entry.path().extension() != ".json") continue;

        // load and process the json
        std::ifstream f(entry.path());
        json data = json::parse(f);

        if (data["Type"] != "Asset") continue;
        if (data["AssetType"] != "Tilemap") continue;

        load_tilemap(scene, data);
    }
}

// -------------- SINGLE SAVE
bool ResLoader::save_texture(const std::string& name,
                             const ise::Texture& texture) {
    json data;
    data["Type"] = "Asset";
    data["AssetType"] = "Texture";
    data["Name"] = name;
    data["FileName"] = texture.file_name;

    // NOTE: for now unused
    data["Attributes"]["Rect"]["X1"] = -1;
    data["Attributes"]["Rect"]["Y1"] = -1;
    data["Attributes"]["Rect"]["X2"] = -1;
    data["Attributes"]["Rect"]["Y2"] = -1;

    return save_json(data, assets_path / ASSETS_JSON_STUB / (name + ".json"));
}

bool ResLoader::save_anim_sprite(const std::string& name,
                                 const std::string& tex_name, int width,
                                 int height,
                                 const ise::AnimatedSprite& sprite) {
    json data;
    data["Type"] = "Asset";
    data["AssetType"] = "AnimatedSprite";
    data["Name"] = name;
    data["TextureName"] = tex_name;
    data["Attributes"]["Width"] = width;
    data["Attributes"]["Height"] = height;
    data["Attributes"]["Fps"] = 1.0f;

    return save_json(data, assets_path / ASSETS_JSON_STUB / (name + ".json"));
}

bool ResLoader::save_tile(ise::Tile& tile) {
    json data;
    data["Type"] = "Asset";
    data["AssetType"] = "Tile";
    data["Name"] = tile.name;
    data["SpriteName"] = tile.get_sprite().name;
    data["Attributes"]["TopOffset"] = tile.get_top_offset();
    data["Attributes"]["YOffset"] = tile.get_y_offset();

    return save_json(data,
                     assets_path / ASSETS_JSON_STUB / (tile.name + ".json"));
}

bool ResLoader::save_tilemap(const ise::Tilemap& tilemap) {
    json data;
    data["Type"] = "Asset";
    data["AssetType"] = "Tilemap";
    data["Name"] = tilemap.name;
    data["TilemapType"] = tilemap.get_type();
    data["Attributes"]["TileWidth"] = tilemap.get_tile_width();
    data["Attributes"]["TileHeight"] = tilemap.get_tile_height();
    data["Attributes"]["OriginOffset"] = {0, 0};
    data["Attributes"]["TileData"] = {};

    // Load the tiles
    auto tile_it = tilemap.tiles_begin();

    for (; tile_it != tilemap.tiles_end(); tile_it++) {
        std::pair<int, int> pos = tile_it->first;
        const Tile& tile = tile_it->second;

        data["Attributes"]["TileData"].push_back(
            json::object_t::value_type(int_pair_to_brackets(pos), tile.name));
    }

    return save_json(data,
                     assets_path / ASSETS_JSON_STUB / (tilemap.name + ".json"));
}

std::string ResLoader::replace_filename(std::string s,
                                        const std::string& new_name) {
    // intentional copy of the string
    // find last /
    std::size_t last = s.find_last_of("/");
    s.replace(last, s.size(), new_name);
    return s;
}

//--------- Construct from values
bool ResLoader::construct_anim_sprite(ise::Scene& scene,
                                      const std::string& name,
                                      const std::string& tex_name,
                                      int width /*= 0 */, int height /*= 0*/,
                                      float fps /*= 1.0*/) {
    auto tex_it = scene.textures.find(tex_name);
    // if a texture of that name does not exist
    if (!scene.textures.contains(tex_name)) {
        return false;
    }

    std::unique_ptr<ise::AnimatedSprite> sprite_p =
        std::make_unique<ise::AnimatedSprite>(*tex_it->second, width, height,
                                              fps);
    sprite_p->name = name;

    scene.sprites.insert({name, std::move(sprite_p)});
    return true;
}

bool ResLoader::construct_tile(ise::Scene& scene, const std::string& name,
                               const std::string& sprite_name,
                               int top_offset /*= 0*/, int y_offset /*=0*/) {
    auto sprite_it = scene.sprites.find(sprite_name);
    // if a sprite does not exist
    if (!scene.sprites.contains(sprite_name)) {
        return false;
    }

    std::unique_ptr<ise::Tile> tile_p =
        std::make_unique<ise::Tile>(*sprite_it->second, top_offset, y_offset);
    tile_p->name = name;

    scene.tiles.insert({name, std::move(tile_p)});
    return true;
}

bool ResLoader::construct_tilemap(ise::Scene& scene, const std::string& name,
                                  const std::string& type /* = "Standard" */,
                                  int width /*= 32*/, int height /*= 32*/,
                                  float* origin_offset /*= 0*/) {
    std::unique_ptr<ise::Tilemap> tilemap_p;
    if (type == "Standard") {
        tilemap_p =
            std::make_unique<ise::Tilemap>(width, height, sf::Vector2f(0, 0));
    } else if (type == "Isometric") {
        tilemap_p = std::make_unique<ise::IsoTilemap>(width, height,
                                                      sf::Vector2f(0, 0));
    } else {
        return false;
    }

    tilemap_p->name = name;
    scene.tilemaps.insert({name, std::move(tilemap_p)});
    return true;
}

// DELETE
bool ResLoader::delete_anim_sprite(ise::Scene& scene, const std::string& name) {
    LOG(2, "Deleting sprite: ", name.c_str());
    if (scene.sprites.erase(name) == 0) return false;
    // return if file existed and deleted-
    return std::filesystem::remove(assets_path / ASSETS_JSON_STUB /
                                   (name + ".json"));
}

bool ResLoader::delete_tile(ise::Scene& scene, const std::string& name) {
    LOG(2, "Deleting tile: ", name.c_str());
    if (scene.tiles.erase(name) == 0) return false;
    // return if file existed and deleted-
    return std::filesystem::remove(assets_path / ASSETS_JSON_STUB /
                                   (name + ".json"));
}

bool ResLoader::delete_tilemap(ise::Scene& scene, const std::string& name) {
    LOG(2, "Deleting tilemap: ", name.c_str());
    if (scene.tilemaps.erase(name) == 0) return false;
    // return if file existed and deleted-
    return std::filesystem::remove(assets_path / ASSETS_JSON_STUB /
                                   (name + ".json"));
}

bool ResLoader::set_asset_folder(std::filesystem::path path,
                                 bool error_check /*=true*/) {
    std::filesystem::path assets_path_check = path;
    std::filesystem::path assets_json_path_check = path / ASSETS_JSON_STUB;
    std::filesystem::path assets_img_path_check = path / ASSETS_IMG_STUB;
    std::filesystem::path assets_animated_img_path_check =
        path / ASSETS_ANIMATED_STUB;
    std::filesystem::path assets_font_path_check = path / ASSETS_FONT_STUB;

    bool assets = std::filesystem::is_directory(assets_path_check);
    bool json_dir = std::filesystem::is_directory(assets_json_path_check);
    bool img = std::filesystem::is_directory(assets_img_path_check);
    bool animated =
        std::filesystem::is_directory(assets_animated_img_path_check);
    bool font = std::filesystem::is_directory(assets_font_path_check);

    if (error_check && !(assets && json_dir && img && animated && font)) {
        // not all folders exist do not set
        return false;
    }

    // set the path
    assets_path = assets_path_check;

    // Set project config, if cannot load config keep default one
    std::pair<bool, json> loaded_config =
        load_json(assets_path / "project_config.json");
    if (loaded_config.first) {
        project_config = loaded_config.second;
    }

    // add asset folder to history
    config["PROJECT_HISTORY"].push_back(
        std::filesystem::canonical(assets_path));
    // save config
    save_config();

    return true;
}

bool ResLoader::create_asset_folder(std::filesystem::path path) {
    // if the path isnt an empty directory
    if (!(std::filesystem::is_directory(path) &&
          std::filesystem::is_empty(path)))
        return false;
    std::filesystem::create_directory(path / ASSETS_IMG_STUB);
    std::filesystem::create_directory(path / ASSETS_ANIMATED_STUB);
    std::filesystem::create_directory(path / ASSETS_JSON_STUB);
    std::filesystem::create_directory(path / ASSETS_FONT_STUB);
    return true;
}

std::pair<bool, char*> ResLoader::set_asset_folder_dialog(
    bool error_check /*=true*/) {
    char* path = tinyfd_selectFolderDialog("Choose Asset Folder", "");
    if (path == NULL) return std::make_pair(false, nullptr);
    return std::make_pair(set_asset_folder(path, error_check), path);
}

std::pair<bool, char*> ResLoader::create_asset_folder_dialog() {
    char* path = tinyfd_selectFolderDialog("Choose Asset Folder", "");
    if (path == NULL) return std::make_pair(false, nullptr);
    if (!create_asset_folder(path)) return std::make_pair(false, nullptr);
    // set the folder

    return std::make_pair(set_asset_folder(path), path);
}

void ResLoader::save_screenshot(const sf::RenderWindow& window) {
    sf::Texture screenshot;
    screenshot.create(window.getSize().x, window.getSize().y);
    screenshot.update(window);
    screenshot.copyToImage().saveToFile("shot.png");
}

std::filesystem::path ResLoader::get_path() { return assets_path; }

bool ResLoader::load_config() {
    // Set config, if cannot load config keep default one
    std::pair<bool, json> loaded_config = load_json("config.json");
    if (loaded_config.first) {
        config = loaded_config.second;
    }
    return loaded_config.first;
}

bool ResLoader::save_config() {
    // trim PROJECT_HISTORY to last 5 paths
    if (config["PROJECT_HISTORY"].size() > 5) {
        auto first_it = config["PROJECT_HISTORY"].begin();
        auto last_it = config["PROJECT_HISTORY"].end() - 5;
        config["PROJECT_HISTORY"].erase(first_it, last_it);
    }

    return save_json(config, "config.json");
}

};  // namespace ise
