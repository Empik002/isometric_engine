#pragma once
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/Texture.hpp>
#include <SFML/System/Vector2.hpp>
#include <chrono>
#include <cstddef>
#include <filesystem>
#include <fstream>
#include <map>
#include <memory>
#include <vector>

#include "assets.hpp"
#include "json.hpp"
#include "lib/common/fileDialog/tinyfiledialogs.h"
#include "log.hpp"
#include "scene.hpp"
#include "sf.hpp"
#include "string_util.hpp"

namespace ise {
// TODO: This bs needs a very big rework, but for now it will be made like this
class ResLoader {
    static const std::string DEFAULT_PROJECT_CONFIG_STRING;
    static const std::string DEFAULT_CONFIG_STRING;
    // the file structure of assets folder must be predetermined, so
    // a function to create it will be provided as well
    // the path can be used for automatic loading, or as is in the case
    // of tilemap builder for opening projects
    static std::filesystem::path assets_path;

    static const std::filesystem::path ASSETS_JSON_STUB;
    static const std::filesystem::path ASSETS_IMG_STUB;
    static const std::filesystem::path ASSETS_ANIMATED_STUB;
    static const std::filesystem::path ASSETS_FONT_STUB;

   public:
    static nlohmann::json config;
    static nlohmann::json project_config;

    ResLoader() = delete;

    static std::filesystem::path get_path();

    // --------- SINGLE LOAD
    static bool load_texture_from_png(ise::Scene& scene,
                                      std::filesystem::path path);
    static bool load_texture(ise::Scene& scene, nlohmann::json data);
    static bool load_anim_sprite(ise::Scene& scene, nlohmann::json data);
    static bool load_tile(ise::Scene& scene, nlohmann::json data);
    static bool load_tilemap(ise::Scene& scene, nlohmann::json data);

    static bool load_texture(ise::Scene& scene, std::filesystem::path path);
    static bool load_anim_sprite(ise::Scene& scene, std::filesystem::path path);
    static bool load_tile(ise::Scene& scene, std::filesystem::path path);
    static bool load_tilemap(ise::Scene& scene, std::filesystem::path path);

    // ----------- BULK LOAD
    // the pngs function takes path straight to folder with pngs not asset
    // directory
    static void load_textures_from_pngs(ise::Scene& scene,
                                        std::filesystem::path img_dir_path);
    /*
     *static void load_textures(ise::Scene& scene, std::string asset_dir_path);
     *static void load_anim_sprites(ise::Scene& scene, std::string
     *asset_dir_path); static void load_tiles(ise::Scene& scene, std::string
     *asset_dir_path); static void load_tilemaps(ise::Scene& scene, std::string
     *asset_dir_path);
     */

    // These functuons use the saved asset_path and call the ones above
    static void load_textures(ise::Scene& scene);
    static void load_textures_from_pngs(ise::Scene& scene);
    static void load_anim_sprites(ise::Scene& scene);
    static void load_tiles(ise::Scene& scene);
    static void load_tilemaps(ise::Scene& scene);
    // -------- SINGLE SAVE
    // Create assets from their attributes and save to a json file.
    static bool save_texture(const std::string& name,
                             const ise::Texture& texture);
    static bool save_anim_sprite(const std::string& name,
                                 const std::string& tex_name, int width,
                                 int height, const ise::AnimatedSprite& sprite);
    static bool save_tile(ise::Tile& tile);
    static bool save_tilemap(const ise::Tilemap& tilemap);

    // static sf::Font load_font(std::filesystem::path path);
    // static void load_fonts_from_dir_MAP(
    // std::filesystem::path path, std::map<std::string, sf::Texture>& fon_map);
    // static void load_fonts_from_dir_VEC(
    // std::filesystem::path path, std::vector<sf::Font>& tex_vec);

    // -------------------------

    // ------- Construct from values
    static bool construct_anim_sprite(ise::Scene& scene,
                                      const std::string& name,
                                      const std::string& tex_name,
                                      int width = 0, int height = 0,
                                      float fps = 1.0);
    static bool construct_tile(ise::Scene& scene, const std::string& name,
                               const std::string& sprite_name,
                               int top_offset = 0, int y_offset = 0);
    static bool construct_tilemap(ise::Scene& scene, const std::string& name,
                                  const std::string& type = "Standard",
                                  int width = 32, int height = 32,
                                  float origin_offset[2] = 0);

    // DELETE
    static bool delete_anim_sprite(ise::Scene& scene, const std::string& name);
    static bool delete_tile(ise::Scene& scene, const std::string& name);
    static bool delete_tilemap(ise::Scene& scene, const std::string& name);

    // TODO:: Implement this, it is not implemented
    static bool delete_texture(ise::Scene& scene, const std::string& name);

    // ---------- Asset folder (project create / open)
    // open project
    static bool set_asset_folder(std::filesystem::path path,
                                 bool error_check = true);
    // create project, creates all needed folders / files in the folders
    // specified by path
    static bool create_asset_folder(std::filesystem::path path);
    // opens a file dialog and calls set_asset_folder
    static std::pair<bool, char*> set_asset_folder_dialog(
        bool error_check = true);
    // opens file dialog and calls set_asset_folder
    static std::pair<bool, char*> create_asset_folder_dialog();

    // Other features
    static void save_screenshot(const sf::RenderWindow& window);
    static std::pair<bool, nlohmann::json> load_json(
        std::filesystem::path path);
    static bool save_json(nlohmann::json data, std::filesystem::path path);

    static bool load_config();
    static bool save_config();

   private:
    // intentional string copy
    static std::string replace_filename(std::string s,
                                        const std::string& new_name);
};

}  // namespace ise
