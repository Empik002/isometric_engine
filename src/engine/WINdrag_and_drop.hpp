#pragma once

#include <SFML/Graphics/RenderWindow.hpp>
#include <sf.hpp>
#include <iostream>
#include <Windows.h>
#include <vector>

namespace ise{

using WINDNDState = HWND;

static LONG_PTR window_procedure_pointer = 0x0; 
static std::map<HWND, std::vector<std::string>> WINDNDResults;


/*
 *struct WINDNDState{
 *    HWND window_handle = 0;
 *    static LRESULT CALLBACK callback(HWND handle, UINT message, 
 *            WPARAM wParam, LPARAM lParam);
 *
 *    std::vector<std::string> result_files;
 *    void set_callback();
 *    
 *};
 */


void set_callback(HWND window_handle);

WINDNDState WINDND_init(sf::RenderWindow& window);
std::vector<std::string> WINDND_update(WINDNDState& state);
// TODO: make the exit func
void WINDND_exit(WINDNDState& state);



} // namespace ise
