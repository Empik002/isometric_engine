#pragma once
#include <SFML/System/Vector2.hpp>
#include <cmath>
#include <iostream>
#include <map>

#include "animated_sprite.hpp"
#include "node.hpp"
#include "sf.hpp"

namespace ise {

class Tile : public Node { /*
     * Class for handling Tiles. The AnimatedSprite is copied!!
     *
     * functions:
     *  - set_fps directly sets the fps of the underliying AnimatedSprite
     *    (same with set_position)
     */

    AnimatedSprite _sprite;
    // offset to where the actual tilable base of the sprite is (from the top)
    float _top_offset = 0;
    float _y_offset = 0;

   public:
    // TODO: check if I really want to std::move it...
    Tile(AnimatedSprite sprite, int top_offset = 0, int y_offset = 0)
        : _sprite(std::move(sprite)),
          _top_offset(top_offset),
          _y_offset(y_offset){};
    
    virtual ~Tile(){}; 

    void set_pos(float x, float y);
    void set_pos(sf::Vector2<float> pos);
    sf::Vector2<float> get_pos() const;

    void set_top_offset(float off);
    float get_top_offset() const;

    void set_y_offset(float off);
    float get_y_offset() const;

    void set_fps(float fps);

    AnimatedSprite &get_sprite();

    void update() override;
    void draw(sf::RenderTarget &window) const override;
};

class Tilemap : public Node {
    /*
     * Tilemap Class. The tiles are copied into the tilemap.
     *
     * parameters:
     *  - Tile widht
     *  - Tile height
     *  - (Optional) Origin offset, that will add the offset to all the tiles
     *    NOT IMPLEMENTED (PROBABLY WONT BE)
     *
     *
     * positioning:
     *  - Screen pixel positions are calculated with the functions
     *    x/y_position they take the index of the tile. The indicies are keys
     *    in the std::map and define the tilemap grid.
     *  - Calculating to what tile does a pixel belongs is done with the
     *    x/y_index, this will return the same index for every pixel in the
     *    tile. (So for this normal tilemap it will return 0 for every x from
     *    0 to 50 for tile size 50 and 1 from 50 to 100, same goes for y_index)
     */
   protected:
    int _tile_width;
    int _tile_height;
    sf::Vector2<int> _origin_offset;  // offsets origin from app 0,0
    std::map<std::pair<int, int>, Tile> tiles;

   public:
    Tilemap(int tile_width, int tile_height,
            sf::Vector2<float> origin_offset = {0, 0})
        : _tile_width(tile_width),
          _tile_height(tile_height),
          _origin_offset(origin_offset) {}

    virtual ~Tilemap(){}; 

    int get_tile_width() const;
    int get_tile_height() const;

    std::map<std::pair<int, int>, Tile>::const_iterator tiles_begin() const;
    std::map<std::pair<int, int>, Tile>::const_iterator tiles_end() const;


    void set_tile(int x_index, int y_index, Tile &tile);
    void erase_tile(int x_index, int y_index);
    ise::Tile* get_tile(int x_index, int y_index);

    /*
     * Returns the top left corner pixel position, used to print sprites
     */
    virtual sf::Vector2<int> corner_position(
            int x_index, int y_index, int tile_top_offset) const;

    /*
     * Returns the indices (x, y in the grid) from the position (e.g. mouse pos)
     * The position in the entire grid cell will fall in one index, 
     * example with grid size 10x10 pixels positions (0,0) (6,5) etc will
     * be indices (0,0) but position (11,0) will be (1,0)
     */
    virtual sf::Vector2<int> indices(int x_position, int y_position) const;

    virtual const std::string get_type() const;

    void draw(sf::RenderTarget &window) const override;
    // alternative draw function that merges the tiles with override tiles 
    // so that "ghost" / "fake" tiles can be drawn without drawing over 
    // normal tiles
    void draw(sf::RenderTarget &window, 
            std::map<std::pair<int,int>, Tile>& override_tiles) const;
    // render part of tilemap
    static void draw(sf::RenderTarget &window, 
            std::map<std::pair<int, int>, Tile>::const_iterator begin,
            std::map<std::pair<int, int>, Tile>::const_iterator end);

    void update() override;

};

class IsoTilemap : public Tilemap {
    /*
     * Class for ISOMETRIC Tilemap. Inherits from Tilemap and HIDES
     * functions for calculating tile positions, uses the same constructor.
     */
    float determinant() const;

   public:
    using Tilemap::Tilemap;
    virtual ~IsoTilemap(){}; 
    
    sf::Vector2<int> corner_position(
            int x_index, int y_index, int tile_top_offset) const override;

    sf::Vector2<int> indices(int x_position, int y_position) const override;
    const std::string get_type() const override;
};

}  // namespace ise
