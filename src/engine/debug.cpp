#include "debug.hpp"
#include <SFML/Graphics/CircleShape.hpp>


namespace ise{
namespace debug {

    void draw_pixel(sf::RenderWindow& window, int x, int y){
        sf::CircleShape dot(1.f);
        dot.setPosition(x, y);
        window.draw(dot);
    }

}// namespace ise::debug
} // namespace ise
