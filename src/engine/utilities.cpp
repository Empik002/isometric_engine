#include "utilities.hpp"
#include <cmath>
#include <SFML/Graphics/RenderWindow.hpp>

namespace ise{
sf::Vector2<unsigned int> get_window_center(const sf::RenderWindow& window){
    sf::Vector2<unsigned int> size = window.getSize();
    return {size.x / 2, size.y / 2};
}

void set_mouse_to_center(const sf::RenderWindow& window){
    sf::Vector2<unsigned int> center = get_window_center(window);
    sf::Mouse::setPosition({int(center.x), int(center.y)}, window);
}

sf::Vector2f adjusted_mouse_pos(sf::RenderWindow& window) {
    return window.mapPixelToCoords(sf::Mouse::getPosition(window));
}

sf::Vector2f round(const sf::Vector2f vector){
     return sf::Vector2f{ std::round(vector.x), std::round(vector.y) };
}

} // namespace ise
