#pragma once
#include <cstddef>
#include <string>
#include <stdexcept>

#include "sf.hpp"

sf::Vector2<int> split_str_to_x_y(const std::string& value);
std::string int_pair_to_brackets(const std::pair<int, int>& value);
