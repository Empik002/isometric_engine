#include "animated_sprite.hpp"

#include <ctime>
#include <iostream>

namespace ise {

AnimatedSprite::AnimatedSprite(sf::Texture& texture, int width, int height,
                               float fps /* = 0 */) {
    // handle undisplayable values, which means whole sprite is to be shown
    if (fps == 0 || height == 0 || width == 0) {
        _sprite.setTexture(texture, true);
        return;
    }
    _width = width;
    _height = height;
    _fps = fps;
    _curr_frame = 0;

    sf::Vector2u size = texture.getSize();
    _frames = size.y / height;

    _sprite.setTexture(texture);
    _sprite.setTextureRect(sf::IntRect(0, 0, width, height));
}

void AnimatedSprite::set_fps(float new_fps) { _fps = new_fps; }

float AnimatedSprite::get_fps() const { return _fps; }

void AnimatedSprite::set_position(float x, float y) {
    _sprite.setPosition(x, y);
}

void AnimatedSprite::set_position(const sf::Vector2f& position) {
    _sprite.setPosition(position);
}

const sf::Vector2f& AnimatedSprite::get_position() const {
    return _sprite.getPosition();
}

void AnimatedSprite::update() {
    if (_fps == 0) return;

    sf::Time diff = clock.getElapsedTime() - last_frame_time;
    if (diff.asSeconds() < (1 / _fps)) return;
    if (_curr_frame >= _frames) _curr_frame = 0;

    sf::IntRect rect(0, _curr_frame * _height, _width, _height);
    _sprite.setTextureRect(rect);
    ++_curr_frame;

    last_frame_time = clock.getElapsedTime();
}

void AnimatedSprite::draw(sf::RenderTarget& window) const {
    window.draw(_sprite);
}

sf::Sprite* AnimatedSprite::get_sprite() { return &_sprite; }

}  // namespace ise
