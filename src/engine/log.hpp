#pragma once
#include <string.h>
#include <cstdarg>
#include <iostream>

#define __FILENAME__ (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)


#ifdef DOLOG
    #define LOG(count, ...) \
        std::cout << "[" << __TIME__ << "] " << __FILENAME__ << " (" << __LINE__ << "): "; log_print(count, __VA_ARGS__); std::cout << std::endl 
#else
    #define LOG(count, ...);
#endif

void log_print(int count, ...);
