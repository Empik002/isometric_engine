#include "drag_and_drop.hpp"
#include <SFML/Graphics/RenderWindow.hpp>
#include <WINdrag_and_drop.hpp>
#include <vector>

namespace ise {

LRESULT CALLBACK callback(HWND handle, UINT message, 
        WPARAM wParam, LPARAM lParam)
{
    if(message == WM_DROPFILES)
    {
        // prepare output vector by clearing last out vector
        std::vector<std::string>& result_files = WINDNDResults[handle];
        result_files.clear();
        // get the drop
        HDROP hdrop = reinterpret_cast<HDROP>(wParam);

        // get the number of files and then put them in a vector of strings
        const UINT filescount = DragQueryFile(hdrop, 0xFFFFFFFF, NULL, 0);
        for(UINT i = 0; i < filescount; ++i)
        {
            // get lenght of filepath
            const UINT bufsize = DragQueryFile(hdrop, i, nullptr, 0);
            std::wstring str;
            str.resize(bufsize + 1);
            //put the file in the buffer
            if(DragQueryFileW(hdrop, i, &str[0], bufsize + 1))
            {
                // convert to normal string
                std::string stdstr;
                sf::Utf8::fromWide(str.begin(), str.end(), std::back_inserter(stdstr));

                // add to output vector
                result_files.push_back(stdstr);
            }
        }
        DragFinish(hdrop);
    }//if WM_DROPFILES
    
    return CallWindowProcW(reinterpret_cast<WNDPROC>(window_procedure_pointer), 
            handle, message, wParam, lParam);
}

void set_callback(HWND window_handle){
    window_procedure_pointer = SetWindowLongPtrW(window_handle, GWLP_WNDPROC,
            reinterpret_cast<LONG_PTR>(callback));
}

WINDNDState WINDND_init(sf::RenderWindow& window){
    HWND win_handle = window.getSystemHandle();
    DragAcceptFiles(win_handle, TRUE);

    set_callback(win_handle);
    
    return win_handle;
}

std::vector<std::string> WINDND_update(WINDNDState& state){
    std::vector<std::string> result_files = WINDNDResults[state];
    WINDNDResults[state].clear();
    return result_files;
}

} // namespace ise
