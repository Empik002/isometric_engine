#pragma once
#include "sf.hpp"
#include "node.hpp"

namespace ise{

class AnimatedSprite: public Node{
    /*
     * Class for handling Animated Sprites, Also used for non animated sprites
     * The frames for the animation are all in one texture, every one under 
     * the previous one. (See /img/animated/ folder for examples, or look
     * at how Minecraft does animated blocks).
     * 
     * !!! To actually animate you need to call the update() function !!!
     *
     * parameters:
     *  # Constructor with one parameter:
     *    - Texture for the sprite.
     *    - This constructor creates Animated sprite, with 0 fps
     *      and the width and height of the actuall texture, basically
     *      just normal sf::Sprite with bloat on top.
     *
     *  # Constructor with 4 parameters
     *    - Texture for the sprite
     *    - Width for the sprite
     *    - Height for the sprite
     *      _width and __height specify the display size of the sprite
     *      number of frames is then calculated from the height of the
     *      texture divided by the display height
     *    - (Optional) Fps for the sprite
     *    - This is an actual Animated sprite that will change frames
     *      (unlsess fps is set to 0)
     *
     * functions:
     *  # Position setting
     *    - The position of the sprite is provided directly by the sf::Sprite
     *    with only a getter / setter in this class
     */
    sf::Sprite _sprite;    
    int _width = 0;
    int _height = 0;
    float _fps = 0;
    int _curr_frame = 0;
    int _frames = 0;
    
    inline static sf::Clock clock;
    sf::Time last_frame_time;

  public:
    AnimatedSprite() = delete;
    virtual ~AnimatedSprite(){}; 
    AnimatedSprite(sf::Texture& texture) : _sprite(texture){};
    AnimatedSprite(sf::Texture& texture, int width, int height, float fps = 0);

    void set_position(float x, float y);
    void set_position(const sf::Vector2f& position);

    const sf::Vector2f& get_position() const;

    void set_fps(float new_fps);
    float get_fps() const;
    
    sf::Sprite* get_sprite();

    void update() override;

    void draw(sf::RenderTarget& window) const override;

};

} // namespace ise
