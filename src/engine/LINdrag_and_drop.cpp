/*
 * EXTRACTED AND MODIFIED FROM: https://github.com/PhilPotter/xlib_xdnd
 * BIG THANKS
 *
 * THIS FOCUSES ONLY ON THE TARGET INTERACTION FOR NOW, NEED TO EXTRACT
 * THE REST IF YOU WANT TO USE THE DRAG AND DROP WITH SFML WINDOW AS SOURCE
 */
#include "LINdrag_and_drop.hpp"

#include <X11/X.h>
#include <X11/Xlib.h>
#include <unistd.h>

#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/System/Sleep.hpp>
#include <SFML/Window/WindowHandle.hpp>

namespace ise {

Display* openDisplay(bool close) {
    static Display* disp = nullptr;

    if (!disp) {
        // create new display for the first time
        disp = XOpenDisplay(nullptr);
        if (!disp) {
            // the display failed the open
            LOG(1, "Display didn't open aborting");
            std::abort();
        }
    }

    if (close) {
        XCloseDisplay(disp);
        disp = nullptr;
    }

    return disp;
}

XDNDRenderWindow_pbase::XDNDRenderWindow_pbase(
    sf::VideoMode mode, const sf::String& title,
    sf::Uint32 style /*= sf::Style::Default*/) {
    // Open connection to Xserver
    Display* disp = openDisplay();
    // get just the pointer for simplicity

    // Display* disp = XOpenDisplay(nullptr);

    int screen;
    screen = DefaultScreen(disp);

    XSetWindowAttributes attributes;

    attributes.background_pixel = BlackPixel(disp, screen);
    attributes.event_mask = KeyPressMask;

    const XWindow wind = XCreateWindow(
        disp, RootWindow(disp, screen), 0, 0, mode.width, mode.height, 0,
        DefaultDepth(disp, screen), InputOutput, DefaultVisual(disp, screen),
        CWBackPixel | CWEventMask, &attributes);

    // the window did not open
    if (wind == 0) {
        std::cout << "no window" << std::endl;
    }

    // Set window title
    if (XStoreName(disp, wind, "Isometric Game") == 0) {
        std::cout << "no name" << std::endl;
    }

    if (XMapWindow(disp, wind) == 0) {
        std::cout << "idk " << std::endl;
    }

    _window_handle = wind;
    std::cout << "base " << _window_handle << " sf "
              << (sf::WindowHandle)_window_handle << std::endl;

    XMapWindow(disp, wind);
    XFlush(disp);
}

void XDNDClearState(XDNDStateMachine& xdndState) {
    Display* disp = xdndState.disp;
    Window wind = xdndState.wind;
    XDNDAtoms atoms = xdndState.atoms;

    // clear but keep the window and display
    xdndState = {.disp = disp, .wind = wind, .atoms = atoms};
}

// This is sent by the target when the exchange has completed
void sendXdndFinished(XDNDStateMachine& xdndState) {
    if (xdndState.xdndExchangeStarted && !xdndState.amISource) {
        // Declare message struct and populate its values
        XEvent message;
        message.xclient.type = ClientMessage;
        message.xclient.display = xdndState.disp;
        message.xclient.window = xdndState.otherWindow;  // target
        message.xclient.message_type = xdndState.atoms.XdndFinished;
        message.xclient.format = 32;
        message.xclient.data.l[0] = xdndState.wind;  // source
        message.xclient.data.l[1] = 1;
        message.xclient.data.l[2] = xdndState.atoms.XdndActionCopy;

        // Send it to target window
        if (XSendEvent(xdndState.disp, xdndState.otherWindow, False, 0,
                       &message) == 0)
            printf("send finished failed\n");
    }
}

// This is sent by the target to the source to say whether or not it will accept
// the drop
void sendXdndStatus(XDNDStateMachine& xdndState, Atom action) {
    if (xdndState.xdndExchangeStarted && !xdndState.amISource) {
        // Declare message struct and populate its values
        XEvent message;
        message.xclient.type = ClientMessage;
        message.xclient.display = xdndState.disp;
        message.xclient.window = xdndState.otherWindow;
        message.xclient.message_type = xdndState.atoms.XdndStatus;
        message.xclient.format = 32;
        message.xclient.data.l[0] = xdndState.wind;
        message.xclient.data.l[1] = 1;  // Sets accept and want position flags

        // Send back window rectangle coordinates and width
        message.xclient.data.l[2] = 0;
        message.xclient.data.l[3] = 0;

        // Specify action we accept
        message.xclient.data.l[4] = action;

        // Send it to target window
        if (XSendEvent(xdndState.disp, xdndState.otherWindow, False, 0,
                       &message) == 0)
            printf("send status failed\n");
    }
}

bool doWeAcceptAtom(Atom a, XDNDStateMachine& xdndState) {
    for (std::size_t i = 0; i < xdndState.atoms.typesWeAccept.size(); ++i) {
        if (a == xdndState.atoms.typesWeAccept[i]) {
            return true;
        }
    }

    return false;
}

// This gets the XdndTypeList from the source window when we need it, and then
// determines the type we will ask for
Atom getSupportedType(XDNDStateMachine& xdndState) {
    // Try to get XdndTypeList property
    Atom retVal = None;
    Atom actualType = None;
    int actualFormat;
    unsigned long numOfItems, bytesAfterReturn;
    unsigned char* data = NULL;
    if (XGetWindowProperty(xdndState.disp, xdndState.otherWindow,
                           xdndState.atoms.XdndTypeList, 0, 1024, False,
                           AnyPropertyType, &actualType, &actualFormat,
                           &numOfItems, &bytesAfterReturn, &data) == Success) {
        if (actualType != None) {
            Atom* supportedAtoms = (Atom*)data;
            for (unsigned long int i = 0; i < numOfItems; ++i) {
                if (doWeAcceptAtom(supportedAtoms[i], xdndState)) {
                    retVal = supportedAtoms[i];
                    break;
                }
            }

            XFree(data);
        }
    }

    return retVal;
}

// Read copied path string from our window property
std::vector<std::string> getCopiedData(XDNDStateMachine& xdndState) {
    // Try to get PRIMARY property
    Atom actualType = None;
    int actualFormat;
    unsigned long numOfItems, bytesAfterReturn;
    unsigned char* data = nullptr;

    // if we didnt get the property then just end
    if (XGetWindowProperty(xdndState.disp, xdndState.wind,
                           xdndState.atoms.XDND_DATA, 0, 1024, False,
                           AnyPropertyType, &actualType, &actualFormat,
                           &numOfItems, &bytesAfterReturn, &data) != Success) {
        return {};
    }

    // copy from X buffer and free the X buffer
    // we need to reinterpret_cast because the X function takes unsigned
    // char
    std::string property =
        std::string(reinterpret_cast<char*>(data), numOfItems);
    XFree(data);

    // split all the paths to separate and ignore any that dont start with
    // file://  (we are only interested in files and directories)

    std::vector<std::string> pathList;
    std::size_t start_char = 0;

    while (start_char <= property.length()) {
        // find first newline (they separate the entries)
        std::size_t newline = property.find_first_of("\n", start_char);
        std::string path_str =
            property.substr(start_char, newline - start_char);
        // set start to past the newline

        if (newline != std::string::npos) {
            start_char = newline + 1;
        } else {
            start_char = std::string::npos;
        }

        // check if the substring is a file path
        // and add it to the list otherwise ignore it
        if (path_str.starts_with("file://")) {
            pathList.push_back(path_str.substr(7));
        }
    }

    return pathList;
}

void XDNDinit_atoms(Display* disp, XDNDAtoms& atoms) {
    atoms.XdndAware = XInternAtom(disp, "XdndAware", False);
    atoms.XA_ATOM = XInternAtom(disp, "XA_ATOM", False);
    atoms.XdndEnter = XInternAtom(disp, "XdndEnter", False);
    atoms.XdndPosition = XInternAtom(disp, "XdndPosition", False);
    atoms.XdndActionCopy = XInternAtom(disp, "XdndActionCopy", False);
    atoms.XdndLeave = XInternAtom(disp, "XdndLeave", False);
    atoms.XdndStatus = XInternAtom(disp, "XdndStatus", False);
    atoms.XdndDrop = XInternAtom(disp, "XdndDrop", False);
    atoms.XdndSelection = XInternAtom(disp, "XdndSelection", False);
    atoms.XDND_DATA = XInternAtom(disp, "XDND_DATA", False);
    atoms.XdndTypeList = XInternAtom(disp, "XdndTypeList", False);
    atoms.XdndFinished = XInternAtom(disp, "XdndFinished", False);
    atoms.typesWeAccept[0] = XInternAtom(disp, "text/uri-list", False);
    atoms.typesWeAccept[1] = XInternAtom(disp, "UTF8_STRING", False);
    atoms.typesWeAccept[2] = XInternAtom(disp, "TEXT", False);
    atoms.typesWeAccept[3] = XInternAtom(disp, "STRING", False);
    atoms.typesWeAccept[4] =
        XInternAtom(disp, "text/plain;charset=utf-8", False);
    atoms.typesWeAccept[5] = XInternAtom(disp, "text/plain", False);
}

XDNDStateMachine XDNDinit(XDNDRenderWindow& window) {
    // craate state machine
    XDNDStateMachine xdndState;

    // Open connection to Xserver
    Display* disp = openDisplay();

    // initialize atoms
    XDNDinit_atoms(disp, xdndState.atoms);

    // Add XdndAware property to the window
    unsigned int xdndVersion = XDND_PROTOCOL_VERSION;
    XChangeProperty(disp, window.getSystemHandle(), xdndState.atoms.XdndAware,
                    xdndState.atoms.XA_ATOM, 32, (int)PropModeReplace,
                    (const unsigned char*)&xdndVersion, 1);

    // set the global xdnd info
    xdndState.disp = disp;
    xdndState.wind = window.getSystemHandle();
    return xdndState;
}

void XDNDexit(XDNDStateMachine& xdndState) { XCloseDisplay(xdndState.disp); }

// TODO: this need finishing, and refactoring (would be useful to
// split it into functions
std::vector<std::string> XDNDupdate(XDNDStateMachine& xdndState) {
    XEvent event;

    //  get an event
    //  if we did then it will copy the appropriate event to the event var
    if (!XCheckTypedWindowEvent(xdndState.disp, xdndState.wind, SelectionNotify,
                                &event) &&
        !XCheckTypedWindowEvent(xdndState.disp, xdndState.wind, ClientMessage,
                                &event)) {
        //  If we didnt get an event just return
        return {};
    }

    switch (event.type) {
        case SelectionNotify: {
            // Ignore if not XDND related
            if (event.xselection.property != xdndState.atoms.XDND_DATA) break;

            std::vector<std::string> pathList = getCopiedData(xdndState);

            XDeleteProperty(xdndState.disp, xdndState.wind,
                            xdndState.atoms.XDND_DATA);
            sendXdndFinished(xdndState);
            XDNDClearState(xdndState);

            // return the path
            return pathList;
        }

        case ClientMessage:
            // Check if already in XDND protocol exchange, if we are not:
            if (!xdndState.xdndExchangeStarted) {
                // Only handle XdndEnter messages here
                if (event.xclient.message_type != xdndState.atoms.XdndEnter)
                    break;

                LOG(1, "receiving XdndEnter");

                xdndState.xdndExchangeStarted = true;
                xdndState.amISource = false;
                xdndState.otherWindow = event.xclient.data.l[0];

                // Determine type to ask for
                if (event.xclient.data.l[1] & 0x1) {
                    // More than three types, look in XdndTypeList
                    xdndState.proposedType = getSupportedType(xdndState);
                } else {
                    // Only three types, check three in turn and stop when
                    // we find one we support
                    xdndState.proposedType = None;
                    for (int i = 2; i < 5; ++i) {
                        if (doWeAcceptAtom(event.xclient.data.l[i],
                                           xdndState)) {
                            xdndState.proposedType = event.xclient.data.l[i];
                            break;
                        }
                    }
                }
                break;
                // if we are in XDND protocol exchange
            } else {
                // Check whether we are the target
                if (xdndState.amISource) break;

                // TODO: this probably could be a  switch idk
                if (event.xclient.message_type ==
                    xdndState.atoms.XdndPosition) {
                    // Ignore if not for our window and sent erroneously
                    if (xdndState.xdndPositionReceived &&
                        (Window)event.xclient.data.l[0] !=
                            xdndState.otherWindow) {
                        break;
                    }

                    // Update state
                    xdndState.xdndPositionReceived = true;
                    xdndState.proposedAction = event.xclient.data.l[4];

                    if (!xdndState.xdndStatusSent) {
                        xdndState.xdndStatusSent = true;
                        sendXdndStatus(xdndState, xdndState.proposedAction);
                    }
                }

                else if (event.xclient.message_type ==
                         xdndState.atoms.XdndLeave) {
                    XDNDClearState(xdndState);
                }

                // Check for XdndDrop message
                else if (event.xclient.message_type ==
                         xdndState.atoms.XdndDrop) {
                    // Ignore if not for our window and/or sent erroneously
                    if (!xdndState.xdndPositionReceived ||
                        (Window)event.xclient.data.l[0] !=
                            xdndState.otherWindow) {
                        break;
                    }

                    xdndState.xdndDropTimestamp = event.xclient.data.l[2];

                    XConvertSelection(
                        xdndState.disp, xdndState.atoms.XdndSelection,
                        xdndState.proposedType, xdndState.atoms.XDND_DATA,
                        xdndState.wind, xdndState.xdndDropTimestamp);
                }
            }
            break;
        default:
            break;
    }

    return {};
}
}  // namespace ise
