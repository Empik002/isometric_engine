#include "log.hpp"


void log_print(int count, ...) {
    va_list args;
    va_start(args, count);
    for (int i = 0; i < count; i++){
        std::cout << va_arg(args, char*) << " ";
    } 
    va_end(args);

} 
