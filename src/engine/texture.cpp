#include "texture.hpp"
#include <string>

namespace ise {

Texture::Texture(std::string path){
    loadFromFile(std::move(path));
}

bool Texture::loadFromFile(std::string&& path) {
    std::size_t slash_pos = path.find_last_of("/");
    if (slash_pos != std::string::npos) {
        file_name = path.substr(slash_pos + 1);
    }
    return texture.loadFromFile(path);
}
} /* namespace ise */
