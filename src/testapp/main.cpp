#include <SFML/Window/WindowStyle.hpp>

#include "game.hpp"

int main() {
    sf::RenderWindow window(sf::VideoMode(WINDOW_WIDTH, WINDOW_HEIGHT),
                            "Isometric Game", SCREEN_STYLE);
    Game game(window);

    int out = game.main_loop();
    return out;
}
