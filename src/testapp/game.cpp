#include "game.hpp"

#include "animated_sprite.hpp"
#include <SFML/Graphics/RectangleShape.hpp>

Game::Game(sf::RenderWindow& _window)
    : window(_window),
      camera(sf::FloatRect(0, 0, CAMERA_WIDTH, CAMERA_HEIGHT)) {  // 640, 640
    ise::ResLoader::load_config();

    missing.loadFromFile("assets/img/missing.png");
    font.loadFromFile("assets/fonts/Roboto-Light.ttf");

    test_image_sprite = ise::AnimatedSprite(missing);
    test_button_sprite = ise::AnimatedSprite(missing);

    ui.add_window(test_window);
    test_window.add_widget(test_button);
    test_window.add_widget(test_label);
    test_window.add_widget(test_checkbox);
    test_window.add_widget(test_image);
    test_window.add_widget(test_image_button);

    test_window.set_bounding_box({0,0,500,500});

    ui.add_window(test_window_unmovable);
    test_window_unmovable.add_widget(test_checkbox_unmovable);
    test_window_unmovable.add_widget(test_text_input);

    ui.add_window(test_window2);
    test_window2.add_widget(multiwidget);
}

void Game::move_camera(sf::Event event) {
    static sf::Clock clock;

    const float speed = 0.2;
    const sf::Int32 min_delay = 15;
    if (clock.getElapsedTime().asMilliseconds() < min_delay) return;

    sf::Vector2<unsigned int> center = ise::get_window_center(window);
    int x = (int(center.x) - event.mouseMove.x) * speed;
    int y = (int(center.y) - event.mouseMove.y) * speed;

    camera.move(x, y);

    ise::set_mouse_to_center(window);
    clock.restart();
}

void Game::event_loop() {
    sf::Event event;

    while (window.pollEvent(event)) {
        ui.process_event(event, window);
        if (event.type == sf::Event::Closed) {
            window.close();
        } else if (event.type == sf::Event::Resized) {
            camera.setSize(window.getSize().x, window.getSize().y);
            camera.setSize(event.size.width, event.size.height);
        }

        if (event.type == sf::Event::KeyPressed) {
            pressed_keys.insert(event.key.code);
        } else if (event.type == sf::Event::KeyReleased) {
            check_consume_keys(pressed_keys, event.key.code);
        } else if (event.type == sf::Event::MouseWheelScrolled) {
            camera.zoom((0.03 * (-1 * event.mouseWheelScroll.delta)) + 1);
        } else if (event.type == sf::Event::MouseButtonPressed) {
            if (event.mouseButton.button == sf::Mouse::Right) {
                window.setMouseCursorGrabbed(true);
                window.setMouseCursorVisible(false);
                ise::set_mouse_to_center(window);
            }
        } else if (event.type == sf::Event::MouseButtonReleased) {
            if (event.mouseButton.button == sf::Mouse::Right) {
                window.setMouseCursorGrabbed(false);
                window.setMouseCursorVisible(true);
            }
        } else if (event.type == sf::Event::MouseMoved) {
            // move camera
            if (sf::Mouse::isButtonPressed(sf::Mouse::Right)) {
                move_camera(event);
            }
        }
    }

    process_pressed_keys(pressed_keys);
}

void Game::process_pressed_keys(std::set<sf::Keyboard::Key>& keys) {
    // Quit Shortcut
    if (check_consume_keys(keys, sf::Keyboard::LControl, sf::Keyboard::Q)) {
        window.close();
    }
}

bool Game::check_consume_keys(
    std::set<sf::Keyboard::Key>& keys, sf::Keyboard::Key key_one,
    sf::Keyboard::Key key_two /*= sf::Keyboard::Unknown*/) {
    bool contains_one = keys.contains(key_one);
    // if key_two is left default set to true
    bool contains_two =
        keys.contains(key_two) || key_two == sf::Keyboard::Unknown;

    if (contains_one && contains_two) {
        keys.erase(key_one);
        if (key_two != sf::Keyboard::Unknown) {
            keys.erase(key_two);
        }
        return true;
    }
    return false;
}

int Game::main_loop() {
    while (window.isOpen()) {
        event_loop();
        ui.update(window);

        window.setView(camera);
        window.clear(sf::Color(47, 47, 47));


        ui.draw(window);
        window.display();
    }

    return 0;
}

