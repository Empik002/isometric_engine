#pragma once
#include <algorithm>
#include <array>
#include <cmath>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <random>
#include <set>
#include <string>
#include <vector>

#include "animated_sprite.hpp"
#include "debug.hpp"
#include "log.hpp"
#include "res_loader.hpp"
#include "scene.hpp"
#include "sf.hpp"
#include "ui.hpp"
#include "ui_label.hpp"
#include "ui_multiwidget.hpp"
#include "ui_text_input.hpp"
#include "utilities.hpp"

const int WINDOW_WIDTH = 1000;
const int WINDOW_HEIGHT = 600;

const int CAMERA_WIDTH = 1000;
const int CAMERA_HEIGHT = 600;

const auto SCREEN_STYLE = sf::Style::Default;

class Game {
    sf::RenderWindow &window;
    sf::View camera;

    ise::Scene scene;

    sf::Texture missing;

    sf::Font font;


    sf::Clock delta_clock;
    std::set<sf::Keyboard::Key> pressed_keys;

    // UI:
    sf::Text test_window_text{"Test Window Title", font, 25};
    sf::Text test_button_text{"Test", font, 25};
    sf::Text tes_label_text{"Test Label", font, 35};
    bool test_check_value = false;
    ise::AnimatedSprite test_image_sprite{missing};
    ise::AnimatedSprite test_button_sprite{missing};

    ise::ui::UI ui{window};

    ise::ui::UIWindow test_window{{50, 50, 300, 300}, &test_window_text};
    ise::ui::Button test_button{{5, 5, 75, 45}, test_button_text};
    ise::ui::Label test_label{{5, 55}, tes_label_text};
    ise::ui::CheckBox test_checkbox{{5, 95, 30, 30}, test_check_value};
    ise::ui::Image test_image{{5, 150}, test_image_sprite};
    ise::ui::ImageButton test_image_button{{5, 200, 50, 50}, test_button_sprite};

    bool test_check_unmovable = true;
    ise::ui::UIWindow test_window_unmovable{
        {400, 0, 300, 500}, nullptr, false, {.movebar_height=15}};
    ise::ui::CheckBox test_checkbox_unmovable{{5, 95, 30, 30},
                                              test_check_unmovable};
    // text input test
    std::string out_string;
    ise::ui::TextInput test_text_input{{5, 270, 150, 50}, out_string, 5, {.font=&font}};

    // multiwidget test

    ise::ui::UIWindow test_window2{{300, 50, 300, 300}, &test_window_text};
    ise::ui::TestMultiWidget multiwidget{{0,0}};



    // End UI;

    void event_loop();

    // this function nedd to have its keys set in order of importance
    // because it for example if u press ctrl and Q and B at the same tame
    // and ctrl+Q and ctrl+B are both shortcuts only one will be executed
    // because ctrl will be consumed
    void process_pressed_keys(std::set<sf::Keyboard::Key> &pressed_keys);

    // checks if two keys are in a set, if they are it will remove them
    // from the set and return true, otherwise it wont remove any and return
    // false, if the key_two is left default it will check only one key
    bool check_consume_keys(std::set<sf::Keyboard::Key> &pressed_keys,
                            sf::Keyboard::Key key_one,
                            sf::Keyboard::Key key_two = sf::Keyboard::Unknown);

    /*
     * Moves camera if the approptiate event is triggered and returns TRUE
     * otherwise returns FALSE
     */
    void move_camera(sf::Event event);

   public:
    Game() = delete;
    explicit Game(sf::RenderWindow &_window);

    int main_loop();
};
