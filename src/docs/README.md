# DOCUMENTATION (WIP)

## Intro
This engine takes inspiration from <a href="https://godotengine.org/"> GODOT </a> in designing its object structure.
Godot uses a node based system where everything inherits directly or indirectly from a basic <a href="#api_node">Node</a> class. 
All engine classes / functions are in a **ise** namespace.

## Contents
- <a href="#api">API Class Reference</a>


---

---

## <span id="api">API CLass Reference </span>
 
1. <a href="#api_node">Node</a>
	1. <a href="#api_anisprite">AnimatedSprite</a>
	2. <a href="#api_tile">Tile</a>
	3. <a href="#api_tmap">TileMap</a>
		1. <a href="#api_isomap">IsoTilemap</a>
 2. <a href="#api_resload">ResourceLoader</a>
 3. <a href="#api_scene">Scene</a>
 
---

## <span id="api_anisprite">AnimatedSprite</span>
<a href="#api">Back to reference</a>

#### Public Functions
> - Default constructor is deleted  

> - explicit **AnimatedSprite(** sf::Texture& **texture) : _sprite(texture)**  
Constructor that takes a sf::Texture and creates  sprite with max size (not animated)  

> - **AnimatedSprite(** sf::Texture& **texture**, int **width**, int **height**, float **fps = 0)**  
Constructor that creates an animated sprite with specified dimensions and fps (optional)

> - **void set_position(** float **x** , float **y)**  

> - **void set_position(** const sf::Vector2f& **position)**

> - **const sf::Vector2f& get_position() const**

> - **void set_fps(** float **new_fps)**

> - **float get_fps()**

> - **void update() override**  
Function that runs the animation

> - **void draw(sf::RenderTarget& window) override**  
Draw the sprite

#### Description
Class for handling Animated Sprites, Also used for non animated sprites.
The frames for the animation are all in one texture, every one under the previous one. 
(See /img/animated/ folder for examples, or look at how Minecraft does animated blocks).

**!!! To actually animate you need to call the update() function !!!**

---

## <span id="api_isomap">IsoTilemap</span>
<a href="#api">Back to reference</a>

#### Public Functions
> - Constructor is inherited from <a href="#api_tmap">TileMap</a>

> - **float x_position(** int **x_index**, int **y_index) const override**  
Returns the position where the <a href="#api_tile">Tiles</a> should be placed. Takes the index of the tile (position in tilemap) and returns the position where exactly it is supposed to be placed. (used internally and for other uses like drawing ghost tiles) 

> - **float y_position(int x_index**, int **y_index**, float **top_offset) const override**  
Same as x_position but for Y, top offset is from the tile.

> - **int x_index(**int **x_position**, int **y_position) const**  
The opposite of x/y_position. From the position the index where the position is included is computed (a square)

> - **int y_index(int x_position**, int **y_position) const**  
Same as x_index

#### Description
Class for ISOMETRIC Tilemap. Inherits from <a href="#api_tmap">Tilemap</a> and HIDES functions for calculating tile positions, uses the same constructor.

---

## <span id="api_node">Node</span>
<a href="#api">Back to reference</a>

#### Public Functions
> - Default constructor

> - **Virtual Destructor**

> - **virtual void update()**

> - **virtual void draw(sf::RenderTarget &)**

#### Description
ABSTRACT base class for most of the engine, the functions are not implemented.

## <span id="api_resload">ResourceLoader (TODO)</span>
<a href="#api">Back to reference</a>

## <span id="api_scene">Scene</span>
<a href="#api">Back to reference</a>

#### Public Functions
> - Default constructor

> - **virtual void update()**  
Runst update function for all elements in scene

#### Description
This class **does not** inherit from Node. This is an abstract class. It contains a protected unordered_map for objects to be stored in (with names).

---

## <span id="api_tile">Tile</span>
<a href="#api">Back to reference</a>

#### Public Functions
> - **Tile(AnimatedSprite sprite**, int **top_offset = 0**) : _sprite(std::move(sprite)), _top_offset(top_offset)  
Constructor that creates a tile from  **AnimatedSprite**

> - **void set_pos(**float **x**, float **y)**

> - **void set_pos(**sf::Vector2<float> **pos)**

> - **sf::Vector2<float> get_pos() const**

> - **void set_top_offset(**float **off)**

> - **float get_top_offset() const**

> - **void set_y_offset(**float **off)**

> - **float get_y_offset() const**

> - **void set_fps(**float **fps)**

> - **AnimatedSprite &get_sprite()**

> - **void update() override**

> - **void draw(**sf::RenderTarget &**window) override**

#### Description
Class for handling Tiles. The AnimatedSprite is copied!!

## <span id="api_tmap">TileMap</span>
<a href="#api">Back to reference</a>

    protected:
    int _tile_width;
    int _tile_height;
    sf::Vector2<float> _origin_offset; // offsets origin from app 0,0
    std::map<std::pair<int, int>, Tile> tiles;

    public:
    Tilemap(int tile_width, int tile_height,
            sf::Vector2<float> origin_offset = {0, 0})
        : _tile_width(tile_width), _tile_height(tile_height),
          _origin_offset(origin_offset) {}

    void set_tile(int x_index, int y_index, Tile &tile);
    void erase_tile(int x_index, int y_index);

    float x_positon(int x_index) const;
    float y_position(int y_index) const;

    virtual float x_position(int x_index, int y_index) const;
    virtual float y_position(int x_index, int y_index, float top_offset) const;

    int x_index(int x_position) const;
    int y_index(int y_position) const;

    void draw(sf::RenderTarget &window) override;
    void update() override;

#### Description
Tilemap Class. The tiles are copied into the tilemap.

Positioning:  
Screen pixel positions are calculated with the functions x/y_position they take the index of the tile. The indicies are keys in the std::map and define the tilemap grid.

Calculating to what tile does a pixel belongs is done with the x/y_index, this will return the same index for every pixel in the tile. (So for this normal tilemap it will return 0 for every x from 0 to 50 for tile size 50 and 1 from 50 to 100, same goes for y_index)
