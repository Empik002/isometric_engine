#include "builder_ui.hpp"

#include <SFML/Graphics/RenderWindow.hpp>

#include "imgui.h"
#include "res_loader.hpp"
#include "scene.hpp"


namespace tbui {

// take only keys from std::maps for the dropdown list
template <typename T>
std::vector<std::string> map_keys(const std::map<std::string, T>& map) {
    std::vector<std::string> keys;
    for (auto const& pair : map) {
        keys.push_back(pair.first);
    }

    return keys;
}

// *********************************************
// ************** LIST BOX NAMES ***************
// *********************************************

void ListBoxNames::deselect() { unselected = true; }

// dropdown menu
void ListBoxNames::update(const std::vector<std::string>& names) {
    if (!_label_visible) {
        ImGui::PushItemWidth(-1);
    }

    if (names.size() <= selected_index) selected_index = 0;
    if (names.empty()) unselected = true;
    if (ImGui::BeginListBox(_title.c_str())) {
        // set selected tile name
        if (names.size() == 0 or unselected) {
            selected = "";
        } else {
            selected = names[selected_index];
        }

        for (std::size_t i = 0; i < names.size(); i++) {
            // check if we are currently drawing the selected item
            const bool is_selected = (selected_index == i and !unselected);

            // check if an unselected item was clicked
            if (ImGui::Selectable(names[i].c_str(), is_selected)) {
                unselected = false;
                selected_index = i;
            }

            // highlight the selected item
            if (is_selected and !unselected) {
                ImGui::SetItemDefaultFocus();
            }
        }
        ImGui::EndListBox();
    }
    if (!_label_visible) {
        ImGui::PopItemWidth();
    }
};

std::string ListBoxNames::get_selected() { return selected; }

// *********************************************
// ************ Texture Create *****************
// *********************************************

// NOTE: this one is for future use, right now loading textures
// is done automatically, but it loads only whole texture, so no
// support for spritemaps, this will facilitate that
void TextureCreate::ui_update() {
    ImGui::InputText("Texture Path", &tex_path);
    ImGui::InputText("Texture Name", &tex_name);
    ImGui::InputInt4("Rect", rect);

    if (ImGui::Button("Save")) {
        ise::Texture tex;
        tex.name = tex_name;
        // TODO: change this so it uses conversion to string first, not
        // getting .data() from  an IMGUI object
        tex.loadFromFile(("assets/img/" + tex_path).data());
        // ise::ResLoader::save_texture(tex_path, tex);
    }
}

void TextureCreate::clear() {
    tex_path = "";
    tex_name = "";
    rect[0] = 0;
    rect[1] = 0;
    rect[2] = 0;
    rect[3] = 0;
}

// *********************************************
// ************** Sprite Create *****************
// *********************************************

void AnimSpriteCreate::ui_update(ise::Scene& scene,
                                 bool create_name /*= false*/) {
    // Name input
    ImGui::InputText("Animated Sprite Name", &sprite_name);

    // List of availible textures
    texture_name_picker.update(map_keys(scene.textures));

    // Input for rect, and fps
    ImGui::InputInt2("Width / Height", rect);
    ImGui::InputFloat("FPS", &fps);

    // set default name
    if (create_name) {
        sprite_name = texture_name_picker.get_selected() + "_sprite";
    }

    if (ImGui::Button("Save")) {
        save(scene);
    };
}

void AnimSpriteCreate::save(ise::Scene& scene) {
    // create sprite from set values
    bool constructed = ise::ResLoader::construct_anim_sprite(
        scene, sprite_name, texture_name_picker.get_selected(), rect[0],
        rect[1], fps);

    // use the fact that if constructed fails the rest of the expression wont
    // evaluate
    if (constructed && ise::ResLoader::save_anim_sprite(
                           sprite_name, texture_name_picker.get_selected(),
                           rect[0], rect[1], *scene.sprites.at(sprite_name))) {
        LOG(2, "Saved sprite: ", sprite_name.c_str());
    } else {
        LOG(3, "Error: ", "FAILED to save the sprite: ", sprite_name.c_str());
    }
}

void AnimSpriteCreate::clear() {
    sprite_name = "";
    rect[0] = 0;
    rect[1] = 0;
    fps = 1;
}

// *********************************************
// *************** Tile Create *****************
// *********************************************

void TileCreate::ui_update(ise::Scene& scene, bool create_name /*=false*/) {
    ImGui::InputText("Tile Name", &tile_name);
    sprite_name_picker.update(map_keys(scene.sprites));
    ImGui::InputInt("Top Offset", &top_offset);
    ImGui::InputInt("Y Offset", &y_offset);

    // set default name
    if (create_name) {
        tile_name = sprite_name_picker.get_selected() + "_tile";
    }

    if (ImGui::Button("Save")) {
        save(scene);
    };
}

void TileCreate::save(ise::Scene& scene) {
    bool constructed = ise::ResLoader::construct_tile(
        scene, tile_name, sprite_name_picker.get_selected(), top_offset,
        y_offset);

    // use the fact that if constructed fails the rest of the expression wont
    // evaluate
    if (constructed && ise::ResLoader::save_tile(*scene.tiles.at(tile_name))) {
        LOG(2, "Saved Tile: ", tile_name.c_str());
    } else {
        LOG(3, "Error: ", "FAILED to save the Tile: ", tile_name.c_str());
    }
}

void TileCreate::clear() {
    tile_name = "";
    top_offset = 0;
    y_offset = 0;
}

// *********************************************
// ************* TileMap Create ****************
// *********************************************

void TilemapCreate::ui_update(ise::Scene& scene) {
    ImGui::Text("For now it only saves Isometric tilemaps");
    ImGui::InputText("Tilemap name", &tilemap_name);
    ImGui::InputInt("Tile width", &tile_width);
    ImGui::InputInt("Tile Height", &tile_height);
    ImGui::InputFloat2("Origin Offset", origin_offset);
    ImGui::Checkbox("Is Isometric", &isometric);

    if (ImGui::Button("Create")) {
        create(scene);
    };
    ImGui::SameLine();
    ImGui::Text("You need to save in the tilemap picker");
}

void TilemapCreate::create(ise::Scene& scene) {
    bool constructed = false;
    if (isometric) {
        constructed = ise::ResLoader::construct_tilemap(
            scene, tilemap_name, "Isometric", tile_width, tile_height,
            origin_offset);
    } else {
        constructed = ise::ResLoader::construct_tilemap(
            scene, tilemap_name, "Standard", tile_width, tile_height,
            origin_offset);
    }
    if (constructed) {
        LOG(2, "Created tilemap: ", tilemap_name.c_str());
    } else {
        LOG(2, "Error: FAILED to create tilemap: ", tilemap_name.c_str());
    }
}
// *********************************************
// *************** Asset Create ****************
// *********************************************

void AssetCreateTab::ui_update(ise::Scene& scene) {
    ImGui::Checkbox("Automatically Create Names", &auto_create_names);

    ImGuiTabBarFlags tab_bar_flags = ImGuiTabBarFlags_None;
    if (ImGui::BeginTabBar("Create", tab_bar_flags)) {
        if (ImGui::BeginTabItem("Sprite Create")) {
            ImGui::Text("Zero FPS means only 1 frame.");
            sprite_menu.ui_update(scene, auto_create_names);
            ImGui::EndTabItem();
        }
        if (ImGui::BeginTabItem("Tile Create")) {
            tile_menu.ui_update(scene, auto_create_names);
            ImGui::EndTabItem();
        }
        if (ImGui::BeginTabItem("Tilemap Create")) {
            tilemap_menu.ui_update(scene);
            ImGui::EndTabItem();
        }
        if (ImGui::BeginTabItem("Texture Create")) {
            ImGui::Text("Dont use this it is WIP and not needed rn");
            texture_menu.ui_update();
            ImGui::EndTabItem();
        }
        ImGui::EndTabBar();
    }
}

// *********************************************
// ************* Tile Selector **************
// *********************************************

void TileSelector::ui_update(ise::Scene& scene) {
    tile_name_picker.update(map_keys(scene.tiles));
    if (ImGui::Button("Delete Tile")) {
        ise::ResLoader::delete_tile(scene, get_selected());
    }
}

std::string TileSelector::get_selected() {
    return tile_name_picker.get_selected();
}

void TileSelector::deselect() { tile_name_picker.deselect(); }

// *********************************************
// ************* Sprite Selector **************
// *********************************************

void AnimSpriteSelector::ui_update(ise::Scene& scene) {
    sprite_name_picker.update(map_keys(scene.sprites));
    if (ImGui::Button("Delete Sprite")) {
        ise::ResLoader::delete_anim_sprite(scene, get_selected());
    }
}

std::string AnimSpriteSelector::get_selected() {
    return sprite_name_picker.get_selected();
}

// *********************************************
// ************* Tilemap Selector **************
// *********************************************

void TilemapSelector::ui_update(ise::Scene& scene) {
    tilemap_name_picker.update(map_keys(scene.tilemaps));
    // ignore if nothing is selected
    if (ImGui::Button("Save") && tilemap_name_picker.get_selected() != "") {
        LOG(2, "Saving Tilemap:", tilemap_name_picker.get_selected().c_str());
        ise::ResLoader::save_tilemap(
            *scene.tilemaps.at(tilemap_name_picker.get_selected()));
    }
    ImGui::SameLine();
    if (ImGui::Button("Delete")) {
        ise::ResLoader::delete_tilemap(scene, get_selected());
    }
}

std::string TilemapSelector::get_selected() {
    return tilemap_name_picker.get_selected();
}

// *********************************************
// *************** ConfigEditor ****************
// *********************************************

ConfigEditor::ConfigEditor() { refresh_project_editor(); }

bool ConfigEditor::ui_update() {
    ImGui::InputInt("Tile Top Offset", &project_top_offset);
    ImGui::ColorPicker3("Background Color##MyColor##5", (float*)&bg_color,
                        ImGuiColorEditFlags_PickerHueBar |
                            ImGuiColorEditFlags_NoSidePreview |
                            ImGuiColorEditFlags_DisplayRGB);
    bool saved = ImGui::Button("Save");
    if (saved) {
        save_project_config();
    }
    ImGui::SameLine();
    if (ImGui::Button("Reset")) {
        refresh_project_editor();
    }
    return saved;
}

void ConfigEditor::save_project_config() {
    // set the values to the resloader json object
    ise::ResLoader::project_config["TOP_OFFSET"] = project_top_offset;
    ise::ResLoader::project_config["BG_COLOR"]["R"] = bg_color[0];
    ise::ResLoader::project_config["BG_COLOR"]["G"] = bg_color[1];
    ise::ResLoader::project_config["BG_COLOR"]["B"] = bg_color[2];

    // save the json
    ise::ResLoader::save_json(
        ise::ResLoader::project_config,
        ise::ResLoader::get_path() / "project_config.json");
}

void ConfigEditor::refresh_project_editor() {
    project_top_offset = ise::ResLoader::project_config["TOP_OFFSET"];
    bg_color[0] = ise::ResLoader::project_config["BG_COLOR"]["R"];
    bg_color[1] = ise::ResLoader::project_config["BG_COLOR"]["G"];
    bg_color[2] = ise::ResLoader::project_config["BG_COLOR"]["B"];
}

sf::Color ConfigEditor::get_bg_color() {
    return sf::Color(bg_color[0] * 255, bg_color[1] * 255, bg_color[2] * 255);
}

// *********************************************
// *************** UI **************************
// *********************************************
UI::UI() { tile_selector.deselect(); }

void UI::main_menu(sf::RenderWindow& window, ise::Scene& scene) {
    if (ImGui::BeginMainMenuBar()) {
        // File menu
        if (ImGui::BeginMenu("File")) {
            if (ImGui::MenuItem("New Project", SHORTCUT_NEW)) {
                create_project(scene);
            }
            if (ImGui::MenuItem("Open Project", SHORTCUT_OPEN)) {
                open_project(scene);
            }
            if (ImGui::MenuItem("Refresh Project", SHORTCUT_REFRESH)) {
                refresh_project(scene);
            }
            if (ImGui::MenuItem("Take Screenshot", NULL)) {
                ise::ResLoader::save_screenshot(window);
            }
            if (ImGui::MenuItem("Quit", SHORTCUT_QUIT)) {
                window.close();
            }
            // quit

            ImGui::EndMenu();
        }

        // View menu
        if (ImGui::BeginMenu("View")) {
            if (ImGui::MenuItem("View Selectors", SHORTCUT_SELECTORS,
                                show_selectors)) {
                toggle_selectors();
            }
            if (ImGui::MenuItem("View Create Tabs", SHORTCUT_CREATE,
                                show_create_tabs)) {
                toggle_create_tabs();
            }
            if (ImGui::MenuItem("View Config", NULL, show_config)) {
                toggle_config();
            }
            ImGui::EndMenu();
        }
        // Edit menu
        if (ImGui::BeginMenu("Edit")) {
            if (ImGui::MenuItem("Deselect Tile", SHORTCUT_DESELECT_TILE)) {
                deselect_tile();
            }
            ImGui::EndMenu();
        }

        ImGui::EndMainMenuBar();
    }
}

void UI::config_window() {
    if (!show_config) return;
    if (ImGui::Begin("Config")) {
        config_editor.ui_update();
    }
    ImGui::End();
}

void UI::create_tabs(ise::Scene& scene) {
    if (!show_create_tabs) return;

    if (ImGui::Begin("Create Assets")) {
        create_tab.ui_update(scene);
    }
    ImGui::End();
}

void UI::selectors(ise::Scene& scene) {
    if (!show_selectors) return;

    if (ImGui::Begin("Selectors")) {
        if (ImGui::Button("New Project")) {
            create_project(scene);
        }
        ImGui::SameLine();
        if (ImGui::Button("Open Project")) {
            open_project(scene);
        }
        ImGui::SameLine();
        if (ImGui::Button("Open Last Project")) {
            if (!ise::ResLoader::config["PROJECT_HISTORY"].empty()) {
                ise::ResLoader::set_asset_folder(
                    ise::ResLoader::config["PROJECT_HISTORY"].back());
                refresh_project(scene);
            }
        }
        ImGui::Separator();

        ImGui::Text("Tilemap Selector");
        tilemap_selector.ui_update(scene);
        ImGui::Separator();
        ImGui::Text("Tile Selector");
        tile_selector.ui_update(scene);
        ImGui::Separator();
        ImGui::Text("Sprites");
        sprite_selector.ui_update(scene);
    }
    ImGui::End();
}

void UI::toggle_create_tabs() { show_create_tabs = !show_create_tabs; }
void UI::toggle_selectors() { show_selectors = !show_selectors; }
void UI::toggle_config() { show_config = !show_config; }

std::string UI::get_selected_tile() { return tile_selector.get_selected(); }
std::string UI::get_selected_tilemap() {
    return tilemap_selector.get_selected();
}
void UI::deselect_tile() { tile_selector.deselect(); }
sf::Color UI::get_bg_color() { return config_editor.get_bg_color(); }

bool UI::create_project(ise::Scene& scene) {
    std::pair<bool, char*> path = ise::ResLoader::create_asset_folder_dialog();

    if (path.first) {
        refresh_project_variables(scene);
    }
    return path.first;
}

bool UI::open_project(ise::Scene& scene) {
    std::pair<bool, char*> path = ise::ResLoader::set_asset_folder_dialog();
    if (path.first) {
        refresh_project_variables(scene);
    }
    return path.first;
}

bool UI::refresh_project(ise::Scene& scene) {
    if (!ise::ResLoader::set_asset_folder(ise::ResLoader::get_path())) {
        return false;
    }
    refresh_project_variables(scene);
    return true;
}

void UI::refresh_project_variables(ise::Scene& scene) {
    // reset scene
    scene.clear();
    // bulk load to the scene
    ise::ResLoader::load_textures_from_pngs(scene);
    ise::ResLoader::load_anim_sprites(scene);
    ise::ResLoader::load_tiles(scene);
    ise::ResLoader::load_tilemaps(scene);
    config_editor.refresh_project_editor();

    // TODO: make this into a proper function!!!
    create_tab.tile_menu.top_offset =
        ise::ResLoader::project_config["TOP_OFFSET"];
}

}  // namespace tbui
