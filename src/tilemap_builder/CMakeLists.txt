add_executable(tilemap_builder 
    main.cpp
    game.cpp
    builder_ui.cpp
    )

add_custom_command(TARGET tilemap_builder PRE_BUILD
                   COMMAND ${CMAKE_COMMAND} -E copy_directory
                   ${CMAKE_SOURCE_DIR}/tilemap_builder/assets ${CMAKE_BINARY_DIR}/tilemap_builder/assets)

IF (${BUILD_TARGET} STREQUAL "LIN")
    target_link_libraries(tilemap_builder GUI ENGINE ${SFGRAPHICS} ${SFWINDOW} ${SFSYSTEM} -lGLU -lGL)
ELSE ()
    target_link_libraries(tilemap_builder GUI ENGINE ${SFGRAPHICS} ${SFWINDOW} ${SFSYSTEM} ${SFMAIN})
ENDIF ()

target_include_directories(tilemap_builder PUBLIC ${INCLUDE_DIR})
    #target_link_libraries(tilemap_builder -lsfml-graphics -lsfml-window -lsfml-system -lGLU -lGL)

