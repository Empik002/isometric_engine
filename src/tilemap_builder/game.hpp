#pragma once
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/Texture.hpp>
#include <SFML/System/Vector2.hpp>
#include <SFML/Window/Keyboard.hpp>
#include <SFML/Window/WindowStyle.hpp>
#include <algorithm>
#include <array>
#include <cmath>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <map>
#include <random>
#include <set>
#include <string>
#include <vector>

#include "animated_sprite.hpp"
#include "debug.hpp"
#include "imgui-SFML.h"
#include "imgui.h"
#include "log.hpp"
#include "res_loader.hpp"
#include "scene.hpp"
#include "sf.hpp"
#include "tilemap.hpp"
#include "utilities.hpp"
#include "drag_and_drop.hpp"

#include "builder_ui.hpp"

const int WINDOW_WIDTH = 1000;
const int WINDOW_HEIGHT = 600;

const int CAMERA_WIDTH = 1000;
const int CAMERA_HEIGHT = 600;

const auto SCREEN_STYLE = sf::Style::Default;

class Game {
    /*
     * Class encompassing the game logic.
     * Textures (and tiles) are stored in a map with names for ease of access.
     *
     * functions:
     *  - add_texture loads a texture from file to the texture map.
     *    on error it will load the "missing" texture, that is loaded
     *    on the Game object creation.
     *    (NOTE: for now no other error output is being generated,
     *    some background logging in the future would be best)
     */

    ise::DNDRenderWindow window;
    sf::View camera;

    ise::DND_state dnd_state;
    ise::Scene scene;

    sf::Texture missing;
    ise::Texture highlight_texture{"assets/img/highlight.png"};
    ise::AnimatedSprite highlight_sprite{highlight_texture};
    ise::Tile highlight_tile{highlight_sprite, 24};

    sf::Font font;

    sf::Clock delta_clock;

    bool tile_remove = false;
    ise::Tile selected_tile{ise::AnimatedSprite(missing)};
    ise::Tilemap* selected_tilemap;

    /*---- UI -----*/
    tbui::UI ui; 
    std::set<sf::Keyboard::Key> pressed_keys; 
    /*---- UI End ---*/

    ise::Tilemap* set_default_tilemap();

    void add_game_textures();
    void add_game_tiles();

    void event_loop();
    // this function nedd to have its keys set in order of importance
    // because it for example if u press ctrl and Q and B at the same tame
    // and ctrl+Q and ctrl+B are both shortcuts only one will be executed
    // because ctrl will be consumed
    void process_pressed_keys(std::set<sf::Keyboard::Key>& pressed_keys);

    // checks if two keys are in a set, if they are it will remove them 
    // from the set and return true, otherwise it wont remove any and return
    // false, if the key_two is left default it will check only one key
    bool check_consume_keys(std::set<sf::Keyboard::Key>& pressed_keys,
                            sf::Keyboard::Key key_one,
                            sf::Keyboard::Key key_two = sf::Keyboard::Unknown);

    void place_tile_at_mouse();
    /*
     *void cycle_tile();
     */
    void set_selected_tile();

    /*
     * Moves camera if the approptiate event is triggered and returns TRUE
     * otherwise returns FALSE
     */
    void move_camera(sf::Event event);

    std::map<std::pair<int, int>, ise::Tile> get_ghost_tiles();
    void draw_highlight_tile();
    void populate_tilemap();

    void ui_update();

   public:
    Game();
    int main_loop();
    sf::Vector2f adjusted_mouse_pos();
};
