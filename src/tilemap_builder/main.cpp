#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Window/WindowStyle.hpp>

#include "game.hpp"

int main() {
    //sf::RenderWindow window(sf::VideoMode(WINDOW_WIDTH, WINDOW_HEIGHT),
                            //"Isometric Game", SCREEN_STYLE);

    Game game;

    int out = game.main_loop();
    ImGui::SFML::Shutdown(); // TODO: move this to destructor
    return out;
}
