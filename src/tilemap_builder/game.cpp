#include "game.hpp"
#include "drag_and_drop.hpp"
#include <SFML/Window/VideoMode.hpp>
#include <vector>

Game::Game()
    : window(sf::VideoMode(800,600), "Tilemap Builder"),
      camera(sf::FloatRect(0, 0, CAMERA_WIDTH, CAMERA_HEIGHT)) {  

    // initialize drag and drop

    dnd_state = ise::DND_init(window);

    // initialize imgui
    if (!ImGui::SFML::Init(window)) {
        exit(-1);
    }

    ise::ResLoader::load_config();

    missing.loadFromFile("assets/img/missing.png");
    font.loadFromFile("assets/fonts/Roboto-Light.ttf");

    selected_tilemap = set_default_tilemap();

    set_selected_tile();

}

ise::Tilemap* Game::set_default_tilemap() {
    if (scene.tilemaps.empty()) {
        // if empty create new standard tilemap
        ise::ResLoader::construct_tilemap(scene,
                                          // set the name
                                          ise::ResLoader::get_path().empty()
                                              ? "Create or open a project"
                                              : "default",
                                          "Isometric");
    }
    return ((scene.tilemaps.begin())->second).get();
}

void Game::move_camera(sf::Event event) {
    static sf::Clock clock;

    const float speed = 0.2;
    const sf::Int32 min_delay = 15;
    if (clock.getElapsedTime().asMilliseconds() < min_delay) return;

    sf::Vector2<unsigned int> center = ise::get_window_center(window);
    int x = (int(center.x) - event.mouseMove.x) * speed;
    int y = (int(center.y) - event.mouseMove.y) * speed;

    camera.move(x, y);

    ise::set_mouse_to_center(window);
    clock.restart();
}

void Game::event_loop() {
    // process drag and drop events
    std::vector<std::string> file_list = ise::DND_update(dnd_state);  
    if (!file_list.empty()){
        for (std::string file : file_list){
            std::cout << "Dropped file: " << file << "\n";
        }
    }

    sf::Event event;
    while (window.pollEvent(event)) {
        ImGui::SFML::ProcessEvent(window, event);

        // Imgui Independant events
        if (event.type == sf::Event::Closed) {
            window.close();
        } else if (event.type == sf::Event::Resized) {
            // SET CAMERA ACCORDING TO SIZE OF WINDOW
            camera.setSize(window.getSize().x, window.getSize().y);
            camera.setSize(event.size.width, event.size.height);
        }

        // IMGUI dependant events to be skipped
        if (ImGui::GetIO().WantCaptureMouse) continue;  // skip

        if (event.type == sf::Event::KeyPressed) {
            pressed_keys.insert(event.key.code);
        } else if (event.type == sf::Event::KeyReleased) {
            check_consume_keys(pressed_keys, event.key.code);
        } else if (event.type == sf::Event::MouseWheelScrolled) {
            camera.zoom((0.03 * (-1 * event.mouseWheelScroll.delta)) + 1);
        } else if (event.type == sf::Event::MouseButtonPressed) {
            if (event.mouseButton.button == sf::Mouse::Right) {
                window.setMouseCursorGrabbed(true);
                window.setMouseCursorVisible(false);
                ise::set_mouse_to_center(window);
            }
        } else if (event.type == sf::Event::MouseButtonReleased) {
            if (event.mouseButton.button == sf::Mouse::Right) {
                window.setMouseCursorGrabbed(false);
                window.setMouseCursorVisible(true);
            }
        } else if (event.type == sf::Event::MouseMoved) {
            // move camera
            if (sf::Mouse::isButtonPressed(sf::Mouse::Right)) {
                move_camera(event);
            }
        }
    }

    process_pressed_keys(pressed_keys);
    if (ImGui::GetIO().WantCaptureMouse) return;  // skip
    if (sf::Mouse::isButtonPressed(sf::Mouse::Left)) place_tile_at_mouse();
}

void Game::process_pressed_keys(std::set<sf::Keyboard::Key>& keys) {
    // Quit Shortcut
    if (check_consume_keys(keys, sf::Keyboard::LControl, sf::Keyboard::Q)) {
        window.close();
    }
    // New Project
    if (check_consume_keys(keys, sf::Keyboard::LControl, sf::Keyboard::N)) {
        ui.create_project(scene);
    }
    // Open Project
    if (check_consume_keys(keys, sf::Keyboard::LControl, sf::Keyboard::O)) {
        ui.open_project(scene);
    }
    if (check_consume_keys(keys, sf::Keyboard::LControl, sf::Keyboard::R)) {
        ui.refresh_project(scene);
    }
    // deselect tile
    if (check_consume_keys(keys, sf::Keyboard::D)) {
        ui.deselect_tile();
    }
    // toggle selectors
    if (check_consume_keys(keys, sf::Keyboard::S)) {
        ui.toggle_selectors();
    }
    // toggle create tab
    if (check_consume_keys(keys, sf::Keyboard::C)) {
        ui.toggle_create_tabs();
    }
}

bool Game::check_consume_keys(
    std::set<sf::Keyboard::Key>& keys, sf::Keyboard::Key key_one,
    sf::Keyboard::Key key_two /*= sf::Keyboard::Unknown*/) {
    bool contains_one = keys.contains(key_one);
    // if key_two is left default set to true
    bool contains_two =
        keys.contains(key_two) || key_two == sf::Keyboard::Unknown;

    if (contains_one && contains_two) {
        keys.erase(key_one);
        if (key_two != sf::Keyboard::Unknown) {
            keys.erase(key_two);
        }
        return true;
    }
    return false;
}

sf::Vector2f Game::adjusted_mouse_pos() {
    return window.mapPixelToCoords(sf::Mouse::getPosition(window));
}

void Game::place_tile_at_mouse() {
    sf::Vector2f pos = adjusted_mouse_pos();

    sf::Vector2<int> indices = selected_tilemap->indices(pos.x, pos.y);
    if (!tile_remove) {
        selected_tilemap->set_tile(indices.x, indices.y, selected_tile);
    } else {
        selected_tilemap->erase_tile(indices.x, indices.y);
    }
}

void Game::set_selected_tile() {
    std::string name = ui.tile_selector.get_selected();
    if (name == "") {
        tile_remove = true;
        return;
    }

    auto find_it = scene.tiles.find(name);
    if (find_it != scene.tiles.end()) {
        tile_remove = false;
        selected_tile = *find_it->second;
    }
}

void Game::draw_highlight_tile() {
    // do not draw if moving
    if (sf::Mouse::isButtonPressed(sf::Mouse::Right)) return;

    sf::Vector2f mouse_pos = adjusted_mouse_pos();

    sf::Vector2<int> indices =
        selected_tilemap->indices(mouse_pos.x, mouse_pos.y);

    sf::Vector2<int> pos = selected_tilemap->corner_position(
        indices.x, indices.y, highlight_tile.get_top_offset());

    // offset by one because the tile is not actually 32x48
    highlight_tile.set_pos(pos.x - 1, pos.y + highlight_tile.get_y_offset());

    highlight_tile.draw(window);
}

std::map<std::pair<int, int>, ise::Tile> Game::get_ghost_tiles() {
    // do not draw if moving
    if (sf::Mouse::isButtonPressed(sf::Mouse::Right) || tile_remove) return {};

    sf::Vector2f mouse_pos = adjusted_mouse_pos();

    sf::Vector2<int> indices =
        selected_tilemap->indices(mouse_pos.x, mouse_pos.y);

    sf::Vector2<int> pos = selected_tilemap->corner_position(
        indices.x, indices.y, selected_tile.get_top_offset());

    selected_tile.set_pos(pos.x, pos.y + selected_tile.get_y_offset());

    std::map<std::pair<int, int>, ise::Tile> ghost_tiles;
    ghost_tiles.insert(
        std::make_pair(std::make_pair(indices.x, indices.y), selected_tile));

    return ghost_tiles;
}

void Game::populate_tilemap() {
    std::size_t len = scene.tiles.size();
    std::vector<std::string> names;
    for (auto iter = scene.tiles.begin(); iter != scene.tiles.end(); ++iter) {
        names.push_back(iter->first);
    }

    for (int i = 0; i < 100; i++) {
        for (int j = 0; j < 100; j++) {
            ise::Tile& tile = *scene.tiles.at(names[rand() % len]);
            selected_tilemap->set_tile(i, j, tile);
        }
    }
}

int Game::main_loop() {
    while (window.isOpen()) {
        event_loop();
        window.setView(camera);
        window.clear(ui.get_bg_color());

        // check if there are no tilemaps, shouldnt happen
        // but to be sure
        if (ui.get_selected_tilemap() != "" &&
            scene.tilemaps.contains(ui.get_selected_tilemap())) {
            selected_tilemap =
                scene.tilemaps.at(ui.get_selected_tilemap()).get();
        } else {
            selected_tilemap = set_default_tilemap();
        }

        set_selected_tile();
        selected_tilemap->update();
        std::map<std::pair<int, int>, ise::Tile> ghost_tiles =
            get_ghost_tiles();
        selected_tilemap->draw(window, ghost_tiles);

        draw_highlight_tile();

        // UI ELEMENTS
        ui_update();

        window.display();
    }
    ise::DND_exit(dnd_state);

    return 0;
}

void Game::ui_update() {
    ImGui::SFML::Update(window, delta_clock.restart());

    ui.main_menu(window, scene);
    ui.selectors(scene);
    ui.create_tabs(scene);
    ui.config_window();
    // ImGui::ShowStackToolWindow(); // NOTE:: For debuging ImGui

    ImGui::SFML::Render(window);
}
