#pragma once
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/System/Vector2.hpp>
#include <iostream>
#include <string>

#include "imgui.h" 
#include "imgui_stdlib.h"
#include "res_loader.hpp"
#include "scene.hpp"

// used for the Shortcut, dont need visual feedback
#include "imgui_internal.h"


namespace tbui {

// ================================
// ===========Shortcuts============
// ================================
const auto SHORTCUT_QUIT="Ctrl+Q";
const auto SHORTCUT_DESELECT_TILE="D";
const auto SHORTCUT_NEW="Ctrl+N";
const auto SHORTCUT_OPEN="Ctrl+O";
const auto SHORTCUT_REFRESH="Ctrl+R";
const auto SHORTCUT_SELECTORS="S";
const auto SHORTCUT_CREATE="C";



// ================================
// ===========UI===================
// ================================

template <typename T>
std::vector<std::string> map_keys(const std::map<std::string, T>& map);

class ListBoxNames {
    std::string _title;
    bool _label_visible;

    std::size_t selected_index = 0;
    bool unselected = true;
    std::string selected;
   public:

    void deselect();
    std::string get_selected();

    ListBoxNames(std::string title = "##", bool label_visible = true) : 
        _title(title), _label_visible(label_visible){};
    void update(const std::vector<std::string>& names);
};




class TextureCreate {
    /*Class that is an imgui window for creating textures*/
   public:
    std::string tex_path;
    std::string tex_name;
    int rect[4] = {0, 0, 0, 0};

    // TODO: this needs to be passed resloader reference,
    void ui_update();
    void clear();
};

class AnimSpriteCreate {
    /*Class that is an imgui window for creating animated sprites*/
    tbui::ListBoxNames texture_name_picker{"Texture List"};
    const char* label = "Textures";

   public:
    std::string sprite_name;
    int rect[2] = {0, 0};
    float fps = 0;

    void ui_update(ise::Scene& scene, bool create_name = false);
    void save(ise::Scene& scene);
    void clear();
};

class TileCreate {
    /*Class that is an imgui window for creating tiles*/
    tbui::ListBoxNames sprite_name_picker{"Sprite List"};
    const char* label = "Sprites";
   public:
    std::string tile_name;
    int top_offset = ise::ResLoader::project_config["TOP_OFFSET"];
    int y_offset = 0;

    void ui_update(ise::Scene& scene, bool create_name = false);
    void save(ise::Scene& scene);
    void clear();
};

class TilemapCreate {
    std::string tilemap_name;
    int tile_width = 32;
    int tile_height = 32;
    float origin_offset[2]={0,0};
    bool isometric = true;
   
  public:

    void ui_update(ise::Scene& scene);
    void clear();
    void create(ise::Scene& scene);
};

class AssetCreateTab {
    bool auto_create_names = true;
    
   public:
    tbui::AnimSpriteCreate sprite_menu;
    tbui::TileCreate tile_menu;
    tbui::TextureCreate texture_menu;
    tbui::TilemapCreate tilemap_menu;


    void ui_update(ise::Scene& scene);
};


class TilemapSelector{
    tbui::ListBoxNames tilemap_name_picker{"##Tilemaps", false};

    bool isometric = true;

    public:
    void deselect();
    std::string get_selected();
    void ui_update(ise::Scene& scene);
    void save(ise::Scene& scene); 
};

class TileSelector{
    tbui::ListBoxNames tile_name_picker{"##Tiles", false};

    public:
    void deselect();
    std::string get_selected();
    void ui_update(ise::Scene& scene);
    void save(ise::Scene& scene); 
};

class AnimSpriteSelector{
    tbui::ListBoxNames sprite_name_picker{"##Sprites",false};

    public:
    std::string get_selected();
    void ui_update(ise::Scene& scene);
    void save(ise::Scene& scene); 
};

class ConfigEditor{
    int project_top_offset = 0;
    float bg_color[3] = {0};

    public:
    ConfigEditor();
    bool ui_update();
    void save_project_config(); 
    //void save_default_config(); 
    void refresh_project_editor(); 
    
    sf::Color get_bg_color();
};

struct UI{
    bool show_ui;

    bool show_create_tabs = false;
    bool show_selectors = true;
    bool show_config = false;

    AssetCreateTab create_tab;
    TilemapSelector tilemap_selector;
    TileSelector tile_selector;
    AnimSpriteSelector sprite_selector;
    ConfigEditor config_editor;
    

    UI();
    
    void create_tabs(ise::Scene& scene);
    void selectors(ise::Scene& scene);
    void main_menu(sf::RenderWindow& window, ise::Scene& scene);
    void config_window();

    void toggle_create_tabs();
    void toggle_selectors();
    void toggle_config();
    
    void deselect_tile();
    std::string get_selected_tile();
    std::string get_selected_tilemap();
    sf::Color get_bg_color();

    bool create_project(ise::Scene& scene);
    bool open_project(ise::Scene& scene);
    bool refresh_project(ise::Scene& scene);
    void refresh_project_variables(ise::Scene& scene);
};

}  // namespace tbui
